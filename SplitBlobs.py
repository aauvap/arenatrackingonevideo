import cv2
import numpy as np
import operator
from operator import itemgetter

class Blob(object):
    def __init__(self, contour, offset = 0, boundingBox = 0, angle = 0, whitePercent = 0, persWeight = 0):
        self.contour = contour 
        self.offset = offset
        self.boundingBox = boundingBox
        self.angle = angle
        self.whitePercent = whitePercent
        self.persWeight = persWeight



class Defect(object):
    def __init__(self, depth, depthPoint, depthAngle, splitAngle, startPoint, endPoint):
        self.depth = depth
        self.depthPoint = depthPoint
        self.depthAngle = depthAngle
        self.splitAngle = splitAngle
        self.startPoint = startPoint
        self.endPoint = endPoint


class Rect(object):
    def __init__(self, x, y, width, height):
        self.x = x
        self.y = y
        self.width = width
        self.height = height


class SplitBlobs(object):
    """This class contains functions to split detected blobs into blobs fitting the persons in the frame.
    
    Functions
    ---------

    """

    def __init__(self, maxHeight):
        self.maxHeight = maxHeight

    def splitAllBlobs(self, origContours, shape):
        """Split blobs if they cover more than one person. Returnes a blobList containing a blob for each person in the frame
        
        Parameters
        ----------
        origContour : contourList
            A list of the contours to be splitted

        Returns
        -------
        blobList
            List containing the final contours
        """

        blobList = []

        finalContours = []

        for contour in origContours:
            x, y, w, h = cv2.boundingRect(contour)

            offset = (x, y)
            contourYMax = y + (h-1)

            # FØLGENDE TROR JEG IKKE BLIVER BRUGT I ORIG. KODE
            #bottomPoints = []

            #for point in contour:
            #    if point[0,1] == contourYMax:
            #        bottomPoints.append(point[0])
            #position = np.mean(np.array(bottomPoints), axis=0)

            #print("The y values should be the same:")
            #print(position)
            #print(contourYMax)

            angle = 90

            blob = Blob(contour, offset, [x, y, w, h], 90)
            
            blobList.append(blob)

        for blob in blobList:
            
            x, y, w, h = cv2.boundingRect(blob.contour)

            blobImage = np.zeros([h+2, w+2], np.uint8)
            blobImage = cv2.drawContours(blobImage, [blob.contour], 0, (255), cv2.FILLED, cv2.LINE_8, None, 1, (-x+1, -y+1))
            tempImg = np.zeros(shape, np.uint8)
            

            didSplitV = True

            while didSplitV:
                didSplitV, blobImage = self.splitBlobVertical(blobImage, blob)
                
            
                #cv2.imshow("blobImageEfterVertical", blobImage)
                #cv2.waitKey(5)

            didSplitH = True

            while didSplitH:
                didSplitH, blobImage = self.splitBlobHorisontal(blobImage, blob)

                #cv2.imshow("blobImage", blobImage)
                #cv2.waitKey(5)

            img, cos, h = cv2.findContours(blobImage, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)

            for co in cos:
                co[:,0,0] = co[:,0,0] + x
                #co[:][:][:][1] = co[:][:][:][1] + x
                co[:,0,1] = co[:,0,1] + y
                finalContours.append(co)


        
        return finalContours


    def splitBlobVertical(self, blobImage, blob):
        didASplit = False
        
        image, contours, hierachy = cv2.findContours(blobImage, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)

        #cv2.imshow("image", image)
        #cv2.waitKey(5)

        # Performs checks whether to split or not
        deviation = 45.
        radianAngle = (blob.angle + 180) * (np.pi / 180)
        radianDeviation = deviation * (np.pi / 180)

        for contour in contours:
            x, y, w, h = cv2.boundingRect(contour)
            position = (x + w/2, y + h)

            # Check split conditions
            if h < self.maxHeight:
                continue

            if cv2.arcLength(contour, True) / ( h *2 + w * 2 ):
                continue

            if (h / w) >= 5:
                continue

            defects = self.findGoodDefects(contour, radianAngle, radianDeviation, False)

            # Go through the list and draw ONE line in the image
            for defect in defects:
                if defect.depth > 1:
                    if ((defect.startPoint[1] < defect.depthPoint[1]) and 
                        (defect.endPoint[1] < defect.depthPoint[1])):
                        didASplit, blobImage = self.split(blobImage, defect)
                        break

        return didASplit, blobImage

    def splitBlobHorisontal(self, blobImage, blob):
        """Split blobs horisontally if they cover more than one person. Returnes an image with splited blobs seperated by a 0 line
        
        Parameters
        ----------
        origContour : contourList
            A list of the contours to be splitted
        blobImage : np.array
            Image with contours for splitting
        blob : Blob
            The blob to be splitted

        Returns
        -------
        bool
            True if the blob was splitted
        np.array
            Image containing the blobs in blobImage splitted by a black line (0-line) if the original blob covered two persons positioned 'atop' of each other
        """

        didASplit = False
        image, contours, hierachy = cv2.findContours(blobImage, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)

        #cv2.imshow("image", image)
        #cv2.waitKey(5)

        #cv2.imshow("blobImage", blobImage)
        #cv2.waitKey(5)

        deviation = 20
        radianAngle = blob.angle * (np.pi / 180) - (np.pi / 2)
        radianDeviation = deviation * (np.pi / 180)

        for contour in contours:
            x, y, w, h = cv2.boundingRect(contour)
            position = (x + w/2, y + h)
            
            if h < self.maxHeight:        
                continue
            defects = self.findGoodDefects(contour, radianAngle, radianDeviation, True)


            # Go through the list and draw ONE line in the image - Øh, splitter den ikke blobne?
            if np.alen(defects) > 0:

                if defects[0].depth > 1:
                    
                    for defect in defects:

                        boundingRect = cv2.boundingRect(contour)
                        
                        # newUpperBlob = cv2.rectangle(image, [x, y], defect.depthPoint)
                        # newLowerBlob = cv2.rectangle(image, defect.depthPoint, [x + w, y + h]) ## Jeg er ikke 100 % sikker på at dette er korrekt.
                        # Jeg forsøger lige med min egen klasse istedet:
                        newUpperBlob = Rect(x, y, w, defect.depthPoint[0][1] - y)
                        newLowerBlob = Rect(x, defect.depthPoint[0][1], w, y+ h - defect.depthPoint[0][1] - y)

                        newUpperPosition = (blob.offset[0] + w / 2, blob.offset[1] + newUpperBlob.height) # Tjek op på om ofset [0] er x eller y
                        # newLowerPosition = (position[0], position[1]) ... bliver jo aldrig brugt!

                        if h > self.maxHeight * 1.1:
                            if newUpperBlob.height < self.maxHeight and newLowerBlob.height < self.maxHeight: # igen her er jeg usikker på om jeg bare må skrive height?
                                if np.abs(defect.depthPoint[0][1] - defect.startPoint[0][1]) + np.abs(defect.depthPoint[0][1] - defect.endPoint[0][1]) <= np.abs(defect.startPoint[0][1] - defect.endPoint[0][1]):
                                    didASplit, blobImage = self.split(blobImage, defect)

                                    return didASplit, blobImage

                    # If we get this far the blob is too high for being 2 persons so we just split
                    for defect in defects:
                        if np.abs(defect.depthPoint[0][1] - defect.startPoint[0][1]) + np.abs(defect.depthPoint[0][1] - defect.endPoint[0][1]) <= np.abs(defect.startPoint[0][1] - defect.endPoint[0][1]):
                            didASplit, blobImage = self.split(blobImage, defects[0])

                            return didASplit, blobImage

        return didASplit, blobImage


    def findGoodDefects(self, contour, angle, deviation, doubleSided):
        """Convex hull: yderpunkterne i et dataset (eller billede) der til sammen ville indeholder hele datasettet.
        convexDefects : "indhak" i datasættet i forhold til det convexe hull. Har et start punkt, farthest point og endepunkt, der tilsammen udgør en trekant depthpoint angiver hvor "dybt" indhakket er.
        
        Parameters
        ----------
        contour : list of tuples
            A list of the contours to be splitted
        angle : float
            The angle of the blob/contour in radians
        deviation : float
            deviation in radians (Hvad er formålet med det?)
        dublesided : bool
            True if the function should chek for horisontal splits. False if testing for vertical splits.
            
        Returns
        -------
        list of defects
            The list of defects indicating that the contour should be splitted and where it should be splitted. If doublesided the the angle in the defect is changed.
        """

        approvedDefects = []
        
        #convexHull = cv2.convexHull(contour, clockwise = True, returnPoints = False)
        convexHull = cv2.convexHull(contour, clockwise = True, returnPoints = False)
        defects = cv2.convexityDefects(contour, convexHull)
        #convexHull = cv2.convexHull(contour, clockwise = True)
        #defects = cv2.convexityDefects(contour, convexHull)
        # https://stackoverflow.com/questions/38518534/messing-with-convexity-defects
        ##### Test af hvad defects smidder ud:
        
        for i in range(defects.shape[0]):
            startDefects, endDefects, farthestPoint, _ = defects[i,0]

        # Sort the defects in descending order according to their depth (element idx 3, axis 2)
        sortedDefects = defects #np.sort(defects[::-1], axis=2)  
        a = defects[::-1]
        b = defects[:,0,3]
        # sortedDefects = np.sort(defects[:,0,3])
        tx, ty, tw, th = cv2.boundingRect(contour)
        #testImg = np.zeros([th + 2, tw + 2], np.uint8)
        testImg = np.ndarray((th + 2, tw + 2, 3))
        testImg = cv2.drawContours(testImg, [contour], 0, (255), cv2.FILLED, cv2.LINE_8, None, 1)
        
        #### DET DER GÅR GALT ER AT SORTERINGSFUNKTIONEN IKKE SORTERER DEFECTS MEN PUNKTERNE INDENI DEFECT - BYTTER RUNDT PÅ ENDPOINT, STARTPOINT, FARTHEST POINT MM


        for i in range(sortedDefects.shape[0]):
            startIdx, endIdx, farthestPointIdx, depth = sortedDefects[i, 0]
            
            #testImg = cv2.circle(testImg, (contour[endIdx][0][0],contour[endIdx][0][1]), 3, (100,100,100)) # hvis/gul ish
            #testImg = cv2.circle(testImg, (contour[startIdx][0][0],contour[startIdx][0][1]), 2, (0,200,0)) # grøn
            #testImg = cv2.circle(testImg, (contour[farthestPointIdx][0][0],contour[farthestPointIdx][0][1]), 2, (0,0,200)) # Rød

            #cv2.imshow("testImg",testImg)
            #cv2.waitKey(5)
            k = 23
            if depth < 1:
                continue

            hullLineX = contour[endIdx][0][0] - contour[startIdx][0][0]
            hullLineY = contour[endIdx][0][1] - contour[startIdx][0][1]
            
            #hullLineX = contour[endIdx,0] - contour[startIdx,0]
            #hullLineY = contour[endIdx,1] - contour[startIdx,1]

            hullAngle = np.arctan2(hullLineY, hullLineX)
            defectAngle = hullAngle - np.pi / 2.
            plusLimit = angle + deviation
            minusLimit = angle - deviation
            
            if defectAngle < -np.pi:
                defectAngle = defectAngle + np.pi * 2
            elif defectAngle > np.pi:
                defectAngle = defectAngle - np.pi * 2
            
            nDefectAngle = 0.
            nPLusLimit = 0.
            nMinusLimit = 0.

            # Investifate if a defect should be appended based on the size of the angle. 
            # The long line of if sentences are used to investigate the angle by taking into account that it can be oriented in direction of different "quadrants"
            if np.abs(defectAngle) > np.pi:
                nDefectAngle = -(defectAngle - np.pi)
            else:
                nDefectAngle = defectAngle - np.pi

            if np.abs(plusLimit) > np.pi:
                nPlusLimit = -(plusLimit - np.pi)
            else:
                nPlusLimit = plusLimit - np.pi

            if np.abs(minusLimit) > np.pi:
                nMinusLimit = -(minusLimit - np.pi)
            else:
                nMinusLimit = minusLimit - np.pi

            if defectAngle > minusLimit and defectAngle < plusLimit:
                approvedDefects.append(Defect(depth, contour[farthestPointIdx], 
                                              defectAngle, angle, contour[startIdx],
                                              contour[endIdx]))
            elif doubleSided:
                sidedDefectAngle = defectAngle
                sidedAngle = angle

                if defectAngle < (np.pi / 2):
                    sidedDefectAngle = defectAngle + np.pi
                    sidedAngle = angle + np.pi
                elif defectAngle > (np.pi * 2):
                    sidedDefectAngle = defectAngle - np.pi
                    sidedAngle = angle - np.pi

                if (sidedDefectAngle < plusLimit and sidedDefectAngle > minusLimit):
                    approvedDefects.append(Defect(depth, contour[farthestPointIdx], 
                                defectAngle, sidedAngle, contour[startIdx],
                                contour[endIdx]))

        approvedDefects = sorted(approvedDefects, key=operator.attrgetter('depth'), reverse = True)
        
        return approvedDefects

    def split(self, image, defect):
        """BESKRIVELSE
        
        Parameters
        ----------
        image: np.array
            
        defect : Defect
            BESKRIVELSE AF HVAD DEN ANVENDES TIL OG EVT OGSÅ HVAD DEN INDEHOLDER OG HVORDAN DEN BRUGES..
            self.depth = depth
            self.depthPoint = depthPoint
            self.depthAngle = depthAngle
            self.splitAngle = splitAngle
            self.startPoint = startPoint
            self.endPoint = endPoint
        blobImage : np.array
            Image with contours for splitting
        blob : Blob
            The blob to be splitted

        Returns
        -------
        list of defects
            A list containing defects. The information in defects can be used to decide where to split a blob GØR BESKRIVELSE FÆRDIG
        """

        #cv2.imshow("image1", image)
        #cv2.waitKey(5)

        drawnInImage = False
        angle = defect.splitAngle
        climb = np.tan(angle) # climb > 1 or climg < -1 if 45 deg < angle < 135 deg. 
        left = False

        if np.abs(angle) > (np.pi / 2):
            left = True

        startX = defect.depthPoint[0][0] # med andre ord også kaldet furthest point x
        startY = defect.depthPoint[0][1]
        x = 0.
        y = 0.
        step = 0.
        delta = 1

        if abs(climb) > 0.00000000000001: # If angle =! 0 or 180 deg (0 or pi in rad)
            delta = np.abs(1./climb) # Jo tættere på en 90 deg vinkel jo større delta

        delta = np.min([delta, 1]) # If delta < 1 delta = delta, else delta = 1

        if left:
            delta = -delta

        lastX = -1 
        lastY = -1

        if np.abs(climb) < 1: # If 45 deg < angle < 135 deg
            bindingDirH = True
        else:
            bindingDirH = False

        pointOK = True

        while pointOK:
            x = startX + step
            y = startY + climb * step # Hvorfor bliver climb lagt til her? det betyder jo at vinklens indflydelse ændres afhængigt af billedstørelsen... Hvordan giver det mening?
            X = (int)(x + 0.5)
            Y = (int)(y + 0.5)
        
            if 0 <= X < image.shape[1] and 0 <= Y < image.shape[0]:
                if lastX is not X or lastY is not Y: # 
                    if image[Y, X] == 255:
                        image[Y, X] = 0
                    else:
                        pointOK = False

                    if bindingDirH: # Skæres vandret
                        tempX = (int)(X - np.sign(delta)) 
                        image[Y, tempX] = 0
                    else: # Skær lodret
                        tempY = (int)(Y - np.sign(climb) * np.sign(delta))
                        image[tempY, X] = 0
    
                    drawnInImage = True
                    
            else:
                pointOK = False
            lastX = X
            lastY = Y
            step += delta

        #cv2.imshow("image2", image)
        #cv2.waitKey(5)
        return drawnInImage, image