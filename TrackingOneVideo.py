import argparse
import cv2
import datetime
import random
import numpy as np

import TrackingUtils as TU
import KalmanUtil as KU

class TrackingOneVideo(object):
    def __init__(self, homography, showVideo):
        
        self.homography = homography
        self.showVideo = showVideo

                
    def startTracking(self, inputVideoPath, backgroundPath):
        cap = cv2.VideoCapture(inputVideoPath)
        backgroundImage = cv2.imread(backgroundPath, 0)
        
        reading = True
        frameNumber = 0
        framesSkipped = False
        kalVec = []
        identity = 0
        distThres = 0.5
        minArea = 100
        blanckFrame = np.ndarray(backgroundImage.shape)
        trackWorldFrame = np.ndarray((1000, 1000, 3))
        dt = 0.1
        fsp = 15 ## ret til... og gør automatisk
        
        # Calibration data, obtained form ?? program:
        temp_H = np.array([[0.010473039767876, 0.0036546492501233, 0.904469365301406],
                             [0.000796841636244571, -0.00967847290267502, 6.71621412070084],
                             [0.0000294928630013532, 0.000627276624950977, 1.0]])
        drawX = 0
        drawY = 0


        while reading:

            reading, inputColorFrame = cap.read()

            if reading == False:
                continue

            inputFrame = cv2.cvtColor(inputColorFrame, cv2.COLOR_BGR2GRAY)
            
            if not reading:
                continue;

            diffFrame = cv2.absdiff(backgroundImage, inputFrame)
        
            #if showVideo:
            #    cv2.imshow("Image", inputFrame)
            #    cv2.imshow("Diff frame", diffFrame)
            #    cv2.waitKey(5)

            th = TU.FindMaximumEntropyThreshold(diffFrame)

            foundBlob = False

            if th < 0:
                framesSkipped = True
            else:
                threshold, thresFrame = cv2.threshold(diffFrame, th, 255, cv2.THRESH_BINARY)
                foundBlob, pixCoord, worldCoord = TU.BlobDetection(thresFrame, minArea, temp_H)

            # If a at least one blob is detected in the frame, chek if a corresponding blob was tracked in the previous frame, otherwise detect a new person/blob
            if foundBlob:
                framesSkipped = False
                kalmanNum = len(kalVec)
                blobNum = len(pixCoord)
                
                # if no blob was detected in the previous frame, make a new tracklet
                if kalmanNum == 0:
                    for pf in worldCoord:
                        colorTxT = (random.randrange(0, 255), random.randrange(0, 255), random.randrange(0, 255))

                        ks = KU.createNewFilter(pf[0], pf[1], colorTxT, identity, dt) # usikker på hvad identity skal være når der oprettes et nyt struct.
                        kalVec.append(ks)
                        identity += 1 
                # If at least one blob was detected in the previous frame chek if it corresponds to the blob in the new frame. 
                # If there is an overlab, so one blob fits with more than one in the other frame, make shure to make the correct combination
                else:
                    for m in range(0, blobNum):
                        minDist = 1000000
                        minNum = -1
                        samples = -1

                        # Investigate if the blobs in the pressent frame is within a reasonable distance of the predicted position of a blob in the previous frame,
                        # and if more than one blob fits to a precicted position, chose the blob closest to the predicted position.
                        for j in range(0, kalmanNum):

                            kalVec[j].kF.predict()

                            dist = np.sqrt(np.power(worldCoord[m][0] - kalVec[j].kF.statePre[0], 2) + np.power(worldCoord[m][1] - kalVec[j].kF.statePre[1], 2)) # der står godtnok at sqrt returnerer et arrray. går det hvis arrayet kun indeholdder 1 nr?
                            
                            if dist < distThres and kalVec[j].samples > samples:
                                minDist = dist
                                minNum = j
                                samples = kalVec[j].samples

                        # add the the blob position to an excisting tracklet.
                        if minDist < distThres:
                            kalVec[minNum].appendMeasNum(m)

                            kalVec[minNum].appendMeasDist(minDist)
                            
                        # If a blob does not fit with a previous detected blob, make a new traclet
                        else:
                            colorTxT = (random.randrange(0, 255), random.randrange(0, 255), random.randrange(0, 255))
                            ks = KU.createNewFilter(pf[0], pf[1], colorTxT, identity, dt)
                            kalVec.append(ks)
                            identity += 1

                    ## svarer ca. til linje 290 i C# koden

                kalmanNum = len(kalVec)

                for j in range(0, kalmanNum):
                    if len(kalVec[j].measNum) == 1:

                        measurement = np.ndarray((2, 1), np.float32)

                        measurement[0] = worldCoord[kalVec[j].measNum[0]][0]
                        measurement[1] = worldCoord[kalVec[j].measNum[0]][1]

                        #imgCoord = pixCoord[kalVec[j].measNum[0]]
                        imgCoord = ((int)(pixCoord[kalVec[j].measNum[0]][0]), (int)(pixCoord[kalVec[j].measNum[0]][1]))
                        #imgCoord[0] = (int)(imgCoord[0])
                        #imgCoord[1] = (int)(imgCoord[1])
                        thresFrame = cv2.circle(thresFrame, imgCoord, 3, kalVec[j].color, -1)

                        kalVec[j].kF.correct(measurement)
                        x = kalVec[j].kF.statePost[0]
                        y = kalVec[j].kF.statePost[1] # Thek op på idholdet af disse værdier. se linje 312 of 313 i C# koden

                        statePt = (int)(100 * (kalVec[j].kF.statePost[0] - drawX)), (int)(100 * (kalVec[j].kF.statePost[1] - drawY))
                        #statePt = (int)(100000 * (kalVec[j].kF.statePost[0] - drawX)), (int)(100000 * (kalVec[j].kF.statePost[1] - drawY))

                        trackWorldFrame = cv2.circle(trackWorldFrame, (statePt[0], trackWorldFrame.shape[1] - statePt[1]), 3, kalVec[j].color)
                        #trackWorldFrame = cv2.circle(trackWorldFrame, (statePt[0], trackWorldFrame.shape[1] + statePt[1]), 5, kalVec[j].color)

                        kalVec[j].deleted = 0
                        kalVec[j].samples += 1

                    elif len(kalVec[j].measNum) > 1:
                        minDist1 = 10000000
                        minNum1 = -1

                        for k in range(0, len(kalVec[j].measNum)):
                            if kalVec[j].measNum[k] < minDist1:
                                minDist1 = kalVec[j].measDist[k]
                                minNum1 = k

                        measurement = np.ndarray((2, 1), np.float32)

                        measurement[0] = worldCoord[kalVec[j].measNum[minNum1]][0]
                        measurement[1] = worldCoord[kalVec[j].measNum[minNum1]][1]

                        imgCoord = ((int)(pixCoord[kalVec[j].measNum[minNum1]][0]), (int)(pixCoord[kalVec[j].measNum[minNum1]][1]))

                        thresFrame = cv2.circle(thresFrame, imgCoord, 3, kalVec[j].color, -1)

                        kalVec[j].kF.correct(measurement)
                        x = kalVec[j].kF.statePost[0]
                        y = kalVec[j].kF.statePost[1]

                        statePt = (int)(100 * (kalVec[j].kF.statePost[0] - drawX)), (int)(100 * (kalVec[j].kF.statePost[1] - drawY))
                        trackWorldFrame = cv2.circle(trackWorldFrame, (statePt[0], trackWorldFrame.shape[1] - statePt[1]), 3, kalVec[j].color)

                        #statePt = (int)(100000 * (kalVec[j].kF.statePost[0] - drawX)), (int)(100000 * (kalVec[j].kF.statePost[1] - drawY))
                        #trackWorldFrame = cv2.circle(trackWorldFrame, (statePt[0], trackWorldFrame.shape[1] + statePt[1]), 1, kalVec[j].color)

                        kalVec[j].deleted = 0
                        kalVec[j].samples += 1

                ## Linje 363 i orig. kode

                kalmanNum = len(kalVec)

                for j in range(0, kalmanNum):
                    if len(kalVec[j].measNum) == 0:
                        pr = kalVec[j].kF.predict()
                        measurementNew = np.ndarray((2, 1), np.float32)

                        measurementNew[0] = kalVec[j].kF.statePre[0]
                        measurementNew[1] = kalVec[j].kF.statePre[1]

                        kalVec[j].deleted += 1
                        
                    KU.KalmanStruct.deleteMeasDist(kalVec[j])
                    KU.KalmanStruct.deleteMeasNum(kalVec[j])

                for j in range(kalmanNum-1, 0 - 1, -1): # tjek op på at det er 0 og ikke 1
                    if kalVec[j].deleted > 8:
                        #kalVec.remove(j)
                        del kalVec[j]

            else:
                framesSkipped = True
                kalmanNum1 = len(kalVec)

                for j in range(kalmanNum1 - 1, 0 - 1, -1): # tjek op
                    if kalVec[j].deleted > 8:
                        #kalVec.remove(j)
                        del kalVec[j]
                    else:
                        kalVec[j].deleted += 1

            if framesSkipped == True:
                thresFrame = blanckFrame

            if frameNumber % (fsp*2) == 0:
                a = 234 # her skulle background opdateres, men den funktion får lige lov til at vente til resten kører...

            frameNumber += 1

            cv2.imshow("trackWorldFrame", trackWorldFrame)
            cv2.waitKey(5)



def readHomography(homographyPath):
    # To be implemented. Currently only returns the identity matrix
    return np.eye(2, 2, 0, np.float32)


parser = argparse.ArgumentParser(description='Tracks people in videos')
parser.add_argument('-v', dest='inputVideo', help='path to the video file')
parser.add_argument('-b', dest='backgroundImage', help='path to the background image file')
parser.add_argument('-o', dest='homography')
parser.add_argument('-s', dest='showVideo', help='Show video while tracking (0, 1)')

args = parser.parse_args()

if args.inputVideo and args.backgroundImage:

    homography = np.eye(2, 2, 0, np.float32)
    if args.homography:
        homography = readHomography(args.homography)

    showVideo = False
    
    if args.showVideo:
        showVideo = bool(args.showVideo)

    tracker = TrackingOneVideo(homography, showVideo)

    tracker.startTracking(args.inputVideo, args.backgroundImage)

else:
    print("Not enough input arguments. Missing -v and -b")