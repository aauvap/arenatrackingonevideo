import numpy as np
import cv2


class KalmanStruct(object):
   def __init__(self, kF, color, label, deleted, samples, measDist = [], measNum = []):
       self.kF = kF
       self.color = color
       self.measDist = measDist
       self.measNum = measNum
       self.label = label
       self.deleted = deleted
       self.samples = samples

#class KalmanStruct(object):
#   def __init__(self, kF, color, measDist, measNum, label, newMeasDist, newMeasNum, deleted, samples):
#       self.kF = kF
#       self.color = color
#       self.measDist = measDist # []
#       # if measDist:
#       #self.measDist.append(measDist)
#       self.measNum = measNum # []
#       #self.measNum.append(measNum)
#       self.label = label
#       self.newMeasDist = newMeasDist
#       self.newMeasNum = newMeasNum
#       self.deleted = deleted
#       self.samples = samples
       
   def appendMeasDist(self, measDist):
 #      if measDist:
           self.measDist.append(measDist)

   def deleteMeasDist(self):
        self.measDist = []

   def appendMeasNum(self, measNum):
 #      if measNum:
           self.measNum.append(measNum)    
           
   def deleteMeasNum(self):
       self.measNum = []


def createNewFilter(xT, yT, colorT, identity, dt):
    """Makes a new kalman filter and returns a kalmanStruct"""
    kF = cv2.KalmanFilter(5, 2, 0)

    kF.transitionMatrix = np.array([[1., 0., dt, 0., 0.],
                                    [0., 1., 0., dt, 0.],
                                    [0., 0., 1., 0., 0.],
                                    [0., 0., 0., 1., 0.],
                                    [0., 0., 0., 0., 1.]], np.float32)

    kF.statePre = np.array([xT, yT, 0., 0., 1.], np.float32)
    kF.statePost = np.array([xT, yT, 0., 0., 1.], np.float32)

    kF.processNoiseCov = np.array([[0.2, 0., 0., 0., 0.],
                                    [0., 0.2, 0., 0., 0.],
                                    [0., 0., 0.3, 0., 0.],
                                    [0., 0., 0., 0.3, 0.],
                                    [0., 0., 0., 0., 0.1]], np.float32)

    kF.measurementNoiseCov = np.array([[0.1, 0],
                                       [0, 0.1]], np.float32)

    kF.measurementMatrix = np.array([[1., 0., 0., 0., 0.],
                                     [0., 1., 0., 0., 0.]], np.float32) 

    kF.errorCovPost = np.array([[0.1, 0., 0., 0., 0.],
                                [0., 0.1, 0., 0., 0.],
                                [0., 0., 0.1, 0., 0.],
                                [0., 0., 0., 0.1, 0.],
                                [0., 0., 0., 0., 0.1]], np.float32)
    # kF.errorCovPre er ikke sat, men det er den hellerikke i originalkoden.

    newFilter = KalmanStruct(kF, colorT, identity, 0, 0)
        #newFilter = KalmanStruct(kF, colorT, identity, 0, 0, 0, 0)
    
    return newFilter