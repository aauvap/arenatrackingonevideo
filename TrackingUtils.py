import cv2
import numpy as np
import SplitBlobs
import random

def FindMaximumEntropyThreshold(inputImage):
    hist = cv2.calcHist([inputImage], [0], None, [256], [0, 255])

    # Calculate total entropy in the image
    total = np.sum(hist)

    normHist = []

    for entry in hist:
        normHist.append(entry / total)

    P1 = [normHist[0]]
    P2 = [1.0 - P1[0]]

    for i in range(1,256):
        P1.append(P1[i-1] + normHist[i]) # P1 gradually increases to unity

    for i in range(1, 256):
        P2.append(P1[255] - P1[i]) # P2 gradually decreases to zero

            
    # Determine the first non-zero bin
    firstBin = 0

    for i in range(0, 255):
        if np.abs(P1[i]) > 0.:
            firstBin = i
            break

    # Determine the last non-zero bin
    lastBin = 255
    for i in range(255, 0, -1):
        if np.abs(P2[i]) > 0.:
            lastBin = i
            break

    # Calculate the total entropy for each gray level
    # and find the threshold that maximizes it
    maxEnt = 2.220446049250313E-16
    threshold = -1

    for i in range(firstBin, lastBin):
        # Entropy of the background pixels
        entBack = 0.0
            
        for j in range(0, i):
            if hist[j] != 0. and P1[i] != 0.:
                entBack -= (normHist[j] / P1[i]) * np.log(normHist[j] / P1[i])

        # Entropy of the object pixels
        entObject = 0.0
            
        for j in range(i+1, 255):
            if hist[j] != 0. and P2[i] != 0.:
                entObject -= (normHist[j] / P2[i]) * np.log(normHist[j] / P2[i])

        # Total entropy
        totalEnt = entBack + entObject

        if maxEnt < totalEnt:
            maxEnt = totalEnt
            threshold = i

    if (maxEnt < 4.1):
        threshold = -1
    elif threshold > 20:
        threshold = 20

    return threshold

def BlobDetection(inputImage, minArea, temp_H):
    counter = 0;

    image, contours, hierachy = cv2.findContours(inputImage, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)

    contList = []
    foundBlob = False;

    pixCoord = []
    worldCoord = []
        
    if contours:
        for contour in contours:
            x, y, w, h = cv2.boundingRect(contour)
            area = w * h
                
            boundingY = y + h

            if (area > minArea):
                counter += 1
                contList.append(contour)

    if counter > 0:
        foundBlob = True
        
        maxHeight = 50
        blobSplitter = SplitBlobs.SplitBlobs(maxHeight)

        # The original blobs are split split if it is most likely that the original blob contains more than one person.
        splitContours = blobSplitter.splitAllBlobs(contList, inputImage.shape)   ################## Kør og tjek dette ud. indholdet i splitContours sejler! #################################
        # Final list should contain a list over all the final blobs in the image
        finalList = contourRect(splitContours, inputImage, inputImage.shape)

        for finalContour in finalList:
            x, y, w, h = cv2.boundingRect(finalContour)
            point = (x + w/2, y + h)

            pixCoord.append(point)
            worldCoordinate = homography(point, temp_H)

            worldCoord.append(worldCoordinate)


    return foundBlob, pixCoord, worldCoord

def updateBackground(backgroundFrame, inputFrame, thresFrame):
    alpha = 0.1
    length = inputFrame.size
    
    for i in range(0, inputFrame.shape[0] - 1):
        for j in range(0, inputFrame.shape[1] - 1):
            if thresFrame[i, j] == 0:
                backgroundFrame[i, j] = (alpha * inputArray[i, j]) + ((1. - alpha) * backgroundImage[i, j] + 0.5)

def contourRect(contList, binaryCropImage, shape):

    finalBlobList = []
    rectList = []
    returnList = []

    contourImg = np.ndarray(shape)
    for contour in contList:
        contourImg = cv2.drawContours(contourImg, [contour], 0, (255), cv2.FILLED, cv2.LINE_8, None, 1)
        #cv2.imshow("contourImg", contourImg)
        #cv2.waitKey(5)

    for contour in contList:

        #bottom = (min(contour[:,0,0]) + (max(contour[:,0,0]) - min(contour[:,0,0])) / 2, min(contour[:,0,1]))
        maxHeight = 50 # skriv maxheight et smart sted så det ikke skal ændres flere steder i koden!!!
        pixHeight = maxHeight
        pixWidth = pixHeight / 2

        bottom = (min(contour[:,0,0]) + (max(contour[:,0,0]) - min(contour[:,0,0])) / 2, max(contour[:,0,1]))

        #rect = SplitBlobs.Rect(bottom[1] - pixWidth / 2, bottom[1] - pixHeight, pixWidth, pixHeight)
        
        x1 = (int)(min(contour[:,0,0]) + (max(contour[:,0,0]) - min(contour[:,0,0])) / 2 - pixWidth / 2)
        x2 = (int)(min(contour[:,0,0]) + (max(contour[:,0,0]) - min(contour[:,0,0])) / 2 + pixWidth / 2)
        y1 = (int)(max(contour[:,0,1]) - pixHeight)
        y2 = (int)(max(contour[:,0,1]))

        # roi = binaryCropImage[min(contour[:,0,1]) : max(contour[:,0,1]), min(contour[:,0,0]) : max(contour[:,0,0])] ## Ret til så det bliver den rigtige firkant der følges!!!
        roi = binaryCropImage[y1 : y2, x1 : x2] ## Ret til så det bliver den rigtige firkant der følges!!!
        #cv2.imshow("roi", roi)
        #cv2.waitKey(5)
     
        edge = False

        whitePix = 0
        whiteEdge = 0

        if (edge == False):
            for x in range(0, roi.shape[1] - 1):
                for y in range(0, roi.shape[0] - 1):
                    if(contourImg[y1 + y, x1 + x] == 255):
                        whitePix += 1
                        if(x == 0 or x == contourImg.shape[1] - 1):
                            whiteEdge += 1
        boxArea = roi.size 

        if boxArea > 0:
            whitePercent = whitePix * 100 / boxArea
            boxPeri = 2 * roi.shape[0] + 2 * roi.shape[1]
            periPercent = whiteEdge * 100 / boxPeri
            
            box = np.zeros(roi.shape, np.uint8)

            randomNum = random.randrange(0,1000)
            name = str(whitePercent) + "-Rect" + str(randomNum) + "-" + ".png" #hmm, så vidt jeg kan se bliver name ikke briugt... tjek op.

            if (edge == False and whitePercent > 15):
                finalBlob = SplitBlobs.Blob(contour, None, None, None, whitePercent, None)
                finalBlobList.append(finalBlob)
                contRect = RectStruct(pixWidth, pixHeight, whitePercent, bottom)
                rectList.append(contRect)

    minOverlap = 45
    deleteList = []

    for i1 in range(0, len(rectList)):
        indexOver = 0
        for i2 in range(0, len(rectList)):
            if (i1 is not i2):
                minOverlap = max(0, (min(rectList[i1].position[0] + rectList[i2].width / 2, rectList[i2].position[0] + rectList[i2].width / 2) - max(rectList[i1].position[0] + rectList[i2].width / 2, rectList[i2].position[0] + rectList[i2].width / 2))) * (min(rectList[i1].position[1], rectList[i2].position[1]) - max(rectList[i1].position[1] - rectList[i1].height, rectList[i2].position[1] - rectList[i2].height))
                if (minOverlap > 0):
                    percent1 = minOverlap / (rectList[i1].height * rectList[i1].width) * 100
                    percent2 = minOverlap / (rectList[i2].height * rectList[i2].width) * 100

                    if (percent1 > minOverlap or percent2 > minOverlap):
                        if (percent1 > percent2):
                            box = cv2.circle(box, rectList[i1].position, 2, (100, 100, 100)) # hvid/gul-ish

                        else:
                            box = cv2.circle(box, rectList[i2].position, 2, (100, 100, 100)) # hvid/gul-ish

                        indexOver = i2

                        if (rectList[i1].ratio > rectList[indexOver].ratio or rectList[i1].ratio == rectList[indexOver].ratio and i1 < indexOver):
                            deleteList.append[indexOver]

    deleteList = sorted(deleteList, reverse = True)
    
    for index in deleteList:
        finalBlobList.remove(index)

    for i in range(0, len(finalBlobList)):
        if (finalBlobList[i].whitePercent <= 20 or finalBlobList[i].whitePercent >= 60):
            finalBlobList[i].persWeight = 0.8

        if ((finalBlobList[i].whitePercent > 20 and finalBlobList[i].whitePercent < 30) or (finalBlobList[i].whitePercent > 50 and finalBlobList[i].whitePercent < 60)):
            finalBlobList[i].persWeight = 0.9

        if (finalBlobList[i].whitePercent >= 30 and finalBlobList[i].whitePercent <= 50):
            finalBlobList[i].persWeight = 1.

        returnList.append(finalBlobList[i].contour)

    return returnList



class RectStruct(object):
    def __init__(self, width, height, ratio, position):
        self.width = width
        self.height = height
        self.ratio = ratio
        self.position = position

def homography(inputCoord, temp_H):
    inputCoordForMultiplication = np.matrix([[inputCoord[0]],
                                            [inputCoord[1]],
                                           [1]])
    callibration = np.matrix(temp_H)
    result = callibration * inputCoordForMultiplication

    wx = result[0] / result[2]
    wy = result[1] / result[2]

    worldCoord = (wx,wy)

    return worldCoord