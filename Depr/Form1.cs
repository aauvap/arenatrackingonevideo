﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.Util;
using System.Xml.Serialization;

namespace TrackingOneVideo
{
    public partial class Form1 : Form
    {
        //Definitions:
        public int imgWidth = 2024; //1440; 
        int imgHeight = 2024; //1080;
        int fps = 25;
        int minArea = 100; //80; 10
        int autoBackFrames = 100; // 100;
        int maxheight = 50; // View 1: 35; 30 - view 2?
        //float[,] temp_H = { { 0.43978891164856249f, 0.763352226210055f, -316.85437202220197f }, { 0.049073072876279988f, -0.333442487842604f, 141.23435638928055f }, { 0.0056587207224363615f, 0.038040027522534069f, 1.0f } };
        
        //KU campus1
        //float[,] temp_H = { { -7.87715710421727f, 89.328242151225f, 76642.1328788575f }, { -0.324414184726658f, 3.18595079534946f, 2731.21831724308f }, { -0.000102940639031126f, 0.00116462017084312f, 1.0f } };
        //int drawX = 76630;
        //int drawY = 2690;

        //KU campus view 2:
        //float[,] temp_H = { { -20.2031842586961f, 250.674404419788f, 76643.1569097081f }, { -0.785190467611207f, 8.87895772267819f, 2769.75634404676f }, { -0.000263011676654867f, 0.00326901709734899f, 1.0f } };
        //int drawX = 76615;
        //int drawY = 2710;

        // Kultorvet view 1
        //float[,] temp_H = { { -16.4635050646485f, 87.2172946034659f, 76489.7806868239f }, { -1.04604257406347f, 5.40698207840463f, 4704.47819428443f }, { -0.000215679717912407f, 0.00113948932033915f, 1.0f } };
        //int drawX = 76480;
        //int drawY = 4670;

        // Kultorvet view 2
        //float[,] temp_H = { { 25.7839380684898f, 354.475088421375f, 76498.2949379214f }, { 1.61425490013798f, 21.9169402387919f, 4630.34378141028f }, { 0.000335371457671866f, 0.00463298830921691f, 1.0f } };
        //int drawX = 76500;
        //int drawY = 4650;

        //Spinderiet dag 2:
        float[,] temp_H = { { 0.010473039767876f, 0.0036546492501233f, 0.904469365301406f }, { 0.000796841636244571f, -0.00967847290267502f, 6.71621412070084f }, { 0.0000294928630013532f, 0.000627276624950977f, 1.0f } };
        int drawX = 0;
        int drawY = 0;
        /*********************************/

        string fileName;
        string videoPath;
        string backgroundName;

        // frames
        private Image<Gray, byte> inputFrame;
        private Image<Gray, byte> thresFrame;
        private Image<Gray, byte> blackFrame;
        private Image<Gray, byte> backgroundFrame;
        private Image<Gray, byte> diffFrame;
        private Image<Rgb, byte> trackworldFrame;
        private bool frameskipped = false;
        private int framenumber = 0;

        private Int32 identity;
        private float distthres;
        private float dt;
        private int kalmannum;
        private List<KalmanStruct> kalvec;

        public Form1()
        {
            InitializeComponent();
        }

        private void chooseVideo_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog ofd = new OpenFileDialog())
            {
                //ofd.Filter = "asf files(.asf)|*.asf";
                if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    videoPath = ofd.FileName;
                    videoPathText.Text = ofd.FileName;
                    fileName = ofd.SafeFileName;
                    autoBackground.Enabled = true;
                    startTracking.Enabled = true;
                }
            }
        }


        private void chooseBackground_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog ofd = new OpenFileDialog())
            {
                ofd.Filter = "png files(.png)|*.png";
                if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    backgroundName = ofd.FileName;
                    backgroundFrame = new Image<Gray, byte>(backgroundName);
                }
            }
        }

        private void autoBackground_Click(object sender, EventArgs e)
        {

            Emgu.CV.Capture cap = new Capture(videoPath);
            double[, ,] background = new double[imgHeight, imgWidth, autoBackFrames/10]; 
            Image<Gray, byte> img;
            backgroundFrame = new Image<Gray, byte>(imgWidth,imgHeight);
            
            for (int i = 0; i < autoBackFrames; i++)
            {
                img = cap.QueryGrayFrame();
                
                if (i % 10 == 0)
                {
                    int j = i / 10;
                    for (int x = 0; x < imgWidth; x++)
                    {
                        for (int y = 0; y < imgHeight; y++)
                        {
                            background[y, x, j] = img[y, x].Intensity;
                        }
                    }
                }
            }

            for (int x = 0; x < imgWidth; x++)
            {
                for (int y = 0; y < imgHeight; y++)
                {
                    double[] temp = new double[autoBackFrames/10];
                    for (int i = 0; i < autoBackFrames/10; i++)
                    {
                        temp[i] = background[y, x, i];

                    }
                    Array.Sort(temp);
                    double median = temp[(int)Math.Floor((decimal)autoBackFrames/10/2)];
                    backgroundFrame[y, x] = new Gray(median);
                }
            }

            CvInvoke.cvShowImage("Background", backgroundFrame);
            
        }

        private void startTracking_Click(object sender, EventArgs e)
        {
            string[] timesplit = fileName.Split('_', '.');
            DateTime time = DateTime.Today; //new DateTime(Convert.ToInt16(timesplit[0].Substring(0, 4)), Convert.ToInt16(timesplit[0].Substring(4, 2)), Convert.ToInt16(timesplit[0].Substring(6, 2)), Convert.ToInt16(timesplit[1].Substring(0, 2)), Convert.ToInt16(timesplit[1].Substring(2, 2)), Convert.ToInt16(timesplit[1].Substring(4, 2)), 0);

            StreamWriter sw = new StreamWriter(@"E:\Fiskeøje Chang\output.txt", true);

            Emgu.CV.Capture cap = new Capture(videoPath);
            bool reading = true;
            
            blackFrame = new Image<Gray, Byte>(imgWidth, imgHeight, new Gray(0.0));
            trackworldFrame = new Image<Rgb, byte>(1000, 1000, new Rgb(0.0, 0.0, 0.0));

            // tracking
            distthres = 0.5f; // 1.0f
            dt = 0.1f;
            kalmannum = 0;
            kalvec = new List<KalmanStruct>();
            identity = 0;
            Random randomInt = new Random();

            Console.WriteLine("START");
            while (reading)
            {

                //if (framenumber > 18000)
                //{
                //    Console.WriteLine("END");
                //    break;
                //}

                double msec = cap.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_POS_MSEC);
                //Console.WriteLine("Framenumber: " + framenumber + " Msec: " + msec);

                //If msec not working properly
                //DateTime timeNew = time.AddMilliseconds(33.3333*framenumber); // If msec working properly - add msec
                //Else
                DateTime timeNew = time.AddMilliseconds(msec);


                sw.WriteLine("Framenumber: " + framenumber + " Timestamp: " + timeNew + ":" + timeNew.Millisecond );

                inputFrame = cap.QueryGrayFrame();
                //inputFrame = inputFrame.Rotate(180, new Gray(0));

                if (debugBox.Checked == true) CvInvoke.cvShowImage("Image", inputFrame);

                
                //CvInvoke.cvWaitKey(50);

                diffFrame = backgroundFrame.AbsDiff(inputFrame);

                if(debugBox.Checked == true) CvInvoke.cvShowImage("Diff frame", diffFrame);
                CvInvoke.cvWaitKey(0);

                double th = MaximumEntropy.FindThreshold(diffFrame); // KU view 1: 40; View 1: 10; Kultorv view 2: 15; //MaximumEntropy.FindThreshold(diffFrame);
                bool foundblob = false;
                List<Point> pixcoord = new List<Point>();
                List<PointF> worldcoord = new List<PointF>();

                //Debug.WriteLine("Threshold: " + th);

                if (th < 0)
                {
                    frameskipped = true;

                }
                else
                {
                    thresFrame = diffFrame.ThresholdBinary(new Gray(th), new Gray(255));

                    Image<Gray, byte> blobframe = new Image<Gray, byte>(imgWidth, imgHeight);
                    BlobDetection(thresFrame, out pixcoord, out worldcoord, out blobframe, out foundblob);
                }

                if (foundblob)
                {
                    frameskipped = false;
                    kalmannum = kalvec.Count();
                    int blobsnum = pixcoord.Count();
                    //Debug.WriteLine("blobsnum: " + blobsnum);
                    //Debug.WriteLine("Kalmannum: " + kalmannum);

                    if (kalmannum == 0)
                    {

                        foreach (PointF pf in worldcoord)
                        {
                            
                            Rgb color = new Rgb(randomInt.Next(255), randomInt.Next(255), randomInt.Next(255));
                            KalmanStruct ks = NewKalman(pf.X, pf.Y, color);
                            kalvec.Add(ks);
                        }

                    }
                    else
                    {
                        for (int m = 0; m < blobsnum; m++)
                        {
                            //Debug.WriteLine("blob m: " + m);
                            double mindist = double.MaxValue;
                            int minnum = -1;
                            int samples = -1;

                            for (int j = 0; j < kalmannum; j++)
                            {
                                kalvec[j].KF.Predict();

                                float dist = (float)Math.Sqrt(Math.Pow(worldcoord[m].X - kalvec[j].KF.PredictedState[0, 0], 2) + Math.Pow(worldcoord[m].Y - kalvec[j].KF.PredictedState[1, 0], 2));
                                
                                //Debug.WriteLine("Kalman j: " + j + " dist: " + dist);

                                if (dist < distthres && kalvec[j].samples > samples)
                                {
                                    mindist = dist;
                                    minnum = j;
                                    samples = kalvec[j].samples;
                                }

                            }
                            if (mindist < distthres)
                            {
                                kalvec[minnum].measdist.Add(mindist);
                                kalvec[minnum].measnum.Add(m);
                                //group.pred?

                            }
                            else
                            {
                                Rgb color = new Rgb(randomInt.Next(255), randomInt.Next(255), randomInt.Next(255));
                                KalmanStruct ks = NewKalman(worldcoord[m].X, worldcoord[m].Y, color);
                                kalvec.Add(ks);
                            }
                        }
                    }

                    kalmannum = kalvec.Count();

                    for (int j = 0; j < kalmannum; j++)
                    {
                        if (kalvec[j].measnum.Count == 1)
                        {
                            Matrix<float> measurement = new Matrix<float>(2, 1);
                            measurement[0, 0] = worldcoord[kalvec[j].measnum[0]].X; //(float)foundgroups[kalvec[j].measnum[0]].groupState[0];
                            measurement[1, 0] = worldcoord[kalvec[j].measnum[0]].Y; //(float)foundgroups[kalvec[j].measnum[0]].groupState[1];

                            Point imgCoord = pixcoord[kalvec[j].measnum[0]];

                            thresFrame.Draw(new CircleF(imgCoord, 5), new Gray(150), -1);
                            
                            kalvec[j].KF.Correct(measurement);
                            double x = (kalvec[j].KF.CorrectedState[0, 0]); //+ 6100000);
                            double y = (kalvec[j].KF.CorrectedState[1, 0]); //+ 720000);
                            sw.WriteLine("ID " + kalvec[j].label + "; " + x + "; " + y);

                            Point statePt = new Point((int)(100 * (float)(kalvec[j].KF.CorrectedState[0, 0] - drawX)), (int)(100 * (float)(kalvec[j].KF.CorrectedState[1, 0] - drawY)));
                            //Console.WriteLine("Point: " + statePt);
                            
                            trackworldFrame.Draw(new CircleF(new PointF(statePt.X, trackworldFrame.Rows - statePt.Y), 1), kalvec[j].color, 1);

                            kalvec[j].deleted = 0;
                            kalvec[j].samples++;

                            // SAVE TRACKINGS (dataformat?)
                            //kalvec[j].measdist.Clear();
                            //kalvec[j].measnum.Clear();

                        }
                        else if (kalvec[j].measnum.Count > 1)
                        {
                            double mindist1 = double.MaxValue;
                            int minnum1 = -1;
                            for (int k = 0; k < kalvec[j].measnum.Count; k++)
                            {
                                if (kalvec[j].measdist[k] < mindist1)
                                {
                                    mindist1 = kalvec[j].measdist[k];
                                    minnum1 = k;
                                }
                            }

                            //Assign closest point to the Kalman filter

                            Matrix<float> measurement = new Matrix<float>(2, 1);
                            measurement[0, 0] = worldcoord[kalvec[j].measnum[minnum1]].X; //(float)foundgroups[kalvec[j].measnum[0]].groupState[0];
                            measurement[1, 0] = worldcoord[kalvec[j].measnum[minnum1]].Y; //(float)foundgroups[kalvec[j].measnum[0]].groupState[1];

                            Point imgCoord = pixcoord[kalvec[j].measnum[minnum1]];

                            thresFrame.Draw(new CircleF(imgCoord, 5), new Gray(150), -1);

                            kalvec[j].KF.Correct(measurement);
                            double x = (kalvec[j].KF.CorrectedState[0, 0]); //+ 6100000);
                            double y = (kalvec[j].KF.CorrectedState[1, 0]); //+ 720000);
                            sw.WriteLine("ID " + kalvec[j].label + "; " + x + "; " + y);

                            Point statePt = new Point((int)(100 * (float)(kalvec[j].KF.CorrectedState[0, 0] - drawX)), (int)(100 * (float)(kalvec[j].KF.CorrectedState[1, 0] - drawY)));
                            //Console.WriteLine("Point: " + statePt);
                            trackworldFrame.Draw(new CircleF(new PointF(statePt.X, trackworldFrame.Rows - statePt.Y), 1), kalvec[j].color, 1);

                            kalvec[j].deleted = 0;
                            kalvec[j].samples++;

                            // Find matches for the rest of the points

                            /*for (int k = 0; k < kalvec[j].measnum.Count; k++)
                            {
                                if (k != minnum1)
                                {
                                    double mindistNew = double.MaxValue;
                                    int minnumNew = -1;

                                    for (int l = 0; l < kalmannum; l++)
                                    {
                                        if (l != j)
                                        {
                                            kalvec[l].KF.Predict();

                                            float dist = (float)Math.Sqrt(Math.Pow(worldcoord[k].X - kalvec[l].KF.PredictedState[0, 0], 2) + Math.Pow(worldcoord[k].Y - kalvec[l].KF.PredictedState[1, 0], 2));

                                            if (dist < mindistNew)
                                            {
                                                mindistNew = dist;
                                                minnumNew = l;
                                            }
                                        }
                                    }
                                    if (mindistNew < distthres && kalvec[minnumNew].measnum.Count() == 0)
                                    {
                                        kalvec[minnumNew].measdist.Add(mindistNew);
                                        kalvec[minnumNew].measnum.Add(k);

                                        Matrix<float> measurementNew = new Matrix<float>(2, 1);
                                        measurementNew[0, 0] = worldcoord[k].X; //(float)foundgroups[kalvec[j].measnum[0]].groupState[0];
                                        measurementNew[1, 0] = worldcoord[k].Y; //(float)foundgroups[kalvec[j].measnum[0]].groupState[1];

                                        Point imgCoordNew = pixcoord[k];

                                        thresFrame.Draw(new CircleF(imgCoordNew, 5), new Gray(150), -1);

                                        kalvec[minnumNew].KF.Correct(measurementNew);
                                        double x1 = (kalvec[minnumNew].KF.CorrectedState[0, 0] + 6100000);
                                        double y1 = (kalvec[minnumNew].KF.CorrectedState[1, 0] + 720000);
                                        sw.WriteLine("ID " + kalvec[minnumNew].label + "; " + x1 + "; " + y1);

                                        Point statePtNew = new Point((int)(10 * (float)(kalvec[j].KF.CorrectedState[0, 0] - drawX)), (int)(10 * (float)(kalvec[j].KF.CorrectedState[1, 0] - drawY)));
                                        //Console.WriteLine("Point: " + statePt);
                                        trackworldFrame.Draw(new CircleF(new PointF(trackworldFrame.Width - statePtNew.Y, statePtNew.X), 1), kalvec[minnumNew].color, 1);
                                        //group.pred?
                                        kalvec[minnumNew].deleted = 0;
                                        kalvec[minnumNew].samples++;

                                    }
                                    else
                                    {
                                        Rgb color = new Rgb(randomInt.Next(255), randomInt.Next(255), randomInt.Next(255));
                                        KalmanStruct ks = NewKalman(worldcoord[k].X, worldcoord[k].Y, color);
                                        kalvec.Add(ks);
                                    }

                                }
                            }*/
                            
                        }
                    }

                    kalmannum = kalvec.Count();
                    for (int j = 0; j < kalmannum; j++)
                    {
                        if (kalvec[j].measnum.Count() == 0)
                        {
                            //sw.WriteLine("ID " + kalvec[j].label + "; " + (kalvec[j].KF.CorrectedState[0, 0] + 6100000) + "; " + (kalvec[j].KF.CorrectedState[1, 0] + 720000));

                            //Point statePtNew = new Point((int)(10 * (float)(kalvec[j].KF.CorrectedState[0, 0] - drawX)), (int)(10 * (float)(kalvec[j].KF.CorrectedState[1, 0] - drawY)));
                            //trackworldFrame.Draw(new CircleF(new PointF(trackworldFrame.Width - statePtNew.Y, statePtNew.X), 1), kalvec[j].color, 1);
                            Matrix<float> pr = kalvec[j].KF.Predict();
                            Matrix<float> measurementNew = new Matrix<float>(2, 1);
                            measurementNew[0, 0] = kalvec[j].KF.PredictedState[0, 0];
                            measurementNew[1, 0] = kalvec[j].KF.PredictedState[1, 0];
                            kalvec[j].KF.Correct(measurementNew);

                            kalvec[j].deleted++;
                        }

                        kalvec[j].measdist.Clear();
                        kalvec[j].measnum.Clear();
                    }

                    for (int j = kalmannum - 1; j >= 0; j--)
                    {
                        if (kalvec[j].deleted > 8)
                        {
                            kalvec.RemoveAt(j);
                            //Debug.WriteLine("DELETE");
                        }
                    }
                }
                else
                {
                    frameskipped = true;
                    int kalmannum1 = kalvec.Count;

                    for (int j = kalmannum1 - 1; j >= 0; j--)
                    {
                        if (kalvec[j].deleted > 8)
                        {
                            kalvec.RemoveAt(j);
                        }
                        else
                        {
                            kalvec[j].deleted++;
                        }

                    }
                }


                if (frameskipped == true)
                {
                    thresFrame = blackFrame;
                }

                if (framenumber % (fps*2) == 0)
                {
                    UpdateBackground();
                }
                framenumber++;

                if (debugBox.Checked) CvInvoke.cvShowImage("Thresframe", thresFrame);
                if (debugBox.Checked) CvInvoke.cvShowImage("TrackImage", trackworldFrame);
                CvInvoke.cvWaitKey(30);
                //if (debugBox.Checked == true) CvInvoke.cvWaitKey(0);
            }
            CvInvoke.cvShowImage("TrackImage", trackworldFrame);
            sw.Close();
            Debug.WriteLine("FINISHED");
        }

        KalmanStruct NewKalman(float x_T, float y_T, Rgb color_T)
        {
            KalmanStruct kalmanS_T = new KalmanStruct();

            kalmanS_T.KF = new Kalman(5, 2, 0);
            kalmanS_T.color = color_T;
            kalmanS_T.label = identity;
            identity++;

            //Debug.WriteLine("NEW Kalman: Identity: {0}", identity, " Label: {0}", kalmanS_T.label);

            kalmanS_T.KF.TransitionMatrix = new Matrix<float>(new float[,] { { 1f, 0f, dt, 0f, 0f }, { 0f, 1f, 0f, dt, 0f }, { 0f, 0f, 1f, 0f, 0f }, { 0f, 0f, 0f, 1f, 0f }, { 0f, 0f, 0f, 0f, 1f } });
            kalmanS_T.KF.PredictedState = new Matrix<float>(new float[] { x_T, y_T, 0f, 0f, 1f });
            kalmanS_T.KF.CorrectedState = new Matrix<float>(new float[] { x_T, y_T, 0f, 0f, 1f });
            kalmanS_T.KF.MeasurementMatrix.SetIdentity();
            kalmanS_T.KF.ProcessNoiseCovariance = new Matrix<float>(new float[,] { { 0.2f, 0f, 0f, 0f, 0f }, { 0f, 0.2f, 0f, 0f, 0f }, { 0f, 0f, 0.3f, 0f, 0f }, { 0f, 0f, 0f, 0.3f, 0f }, { 0f, 0f, 0f, 0f, 0.1f } });
            kalmanS_T.KF.MeasurementNoiseCovariance.SetIdentity(new MCvScalar(0.1));
            kalmanS_T.KF.ErrorCovariancePost.SetIdentity(new MCvScalar(0.1));
            kalmanS_T.deleted = 0;
            kalmanS_T.samples = 0;

            //if (m_T != -1)
            //{
            //    kalmanS_T.measnum.Add(m_T);
            //}

            return kalmanS_T;
        }

        public void UpdateBackground()
        {
            //Console.WriteLine("Update background");

            float alpha = 0.1f;

            int length = imgHeight * imgWidth;

            byte[] inputArray = new byte[length];
            byte[] thresArray = new byte[length];
            byte[] backArray = new byte[length];

            inputArray = inputFrame.Bytes;
            thresArray = thresFrame.Bytes;
            backArray = backgroundFrame.Bytes;

            for (int i = 0; i < length; i++)
            {
                if (thresArray[i] == 0)
                {
                    backArray[i] = (byte)((alpha * inputArray[i]) + ((1.0 - alpha) * backArray[i]) + 0.5);
                }
            }

            backgroundFrame.Bytes = backArray;
            if(debugBox.Checked) CvInvoke.cvShowImage("Background", backgroundFrame);
            //Image<Gray, byte> temp = inputframe.Mul(alpha).Add(backgroundframe.Mul(1.0f - alpha));
            //backgroundframe = (temp.And(thresframe.Not())).Or(backgroundframe.And(thresframe));
        }

        public List<Contour<Point>> ContourRect(List<Contour<Point>> contList, Image<Gray, byte> binaryCropImage)
        {
            //Debug.WriteLine("Start POM processing");
            
            Image<Gray, byte> contourImg = new Image<Gray, byte>(imgWidth, imgHeight, new Gray(0));

            //Image<Gray, UInt16> height = new Image<Gray, ushort>(@"F:\heightImage.png");
            List<RectStruct> RectList = new List<RectStruct>();
            List<Blob> finalList = new List<Blob>();
            List<Contour<Point>> returnList = new List<Contour<Point>>();
            // Draw contours:
            
            //Console.WriteLine("List size: " + contList.Count());

            foreach (Contour<Point> co in contList)
            {
                contourImg.Draw(co, new Gray(255), new Gray(255), 0, -1);
            }
            
            foreach (Contour<Point> co in contList)
            {
                //Debug.WriteLine("Offset: " + bl.Offset);
                
                //contourImg.Draw(bl.Contour, new Gray(255), new Gray(255), 0, -1, bl.Offset);
                Point bottom = new Point(co.BoundingRectangle.Left + co.BoundingRectangle.Width / 2, co.BoundingRectangle.Bottom);
                //contourImg.Draw(new CircleF(bottom, 3), new Gray(100), -1);

                //contourImg.Draw(bl.BoundingBox, new Gray(200), 2);
                
                int pixheight = maxheight;
                int pixwidth = pixheight / 2; //pixheight / 3;
                
                

                Rectangle rect = new Rectangle(bottom.X - pixwidth / 2, bottom.Y - pixheight, pixwidth, pixheight);

                //contourImg.Draw(rect, new Gray(150), 2);


                contourImg.ROI = rect;

                Point rectOffset = new Point(rect.X, rect.Y);
                
                //Debug.WriteLine("Rect offset: " + rectOffset);

                Image<Gray, byte> smallImg = new Image<Gray, byte>(contourImg.ROI.Size);
                contourImg.CopyTo(smallImg);

                //Point blSmallOffset = new Point(rectOffset.X - bl.Offset.X, rectOffset.Y - bl.Offset.Y);
                //Point blSmallOffset = new Point(-rectOffset.X, -rectOffset.Y);
                //smallImg.Draw(bl.Contour, new Gray(255), new Gray(255), 0, -1, blSmallOffset);

                //CvInvoke.cvDestroyWindow("Rect blob");
                //CvInvoke.cvShowImage("Rect blob", smallImg);
                


               

                //contourImg.ROI = rect;

                /*foreach (Blob b2 in BlobList)
                {
                    if (b2.BoundingBox.Bottom < rect.Bottom && b2.BoundingBox.Top > rect.Top)
                    {
                        Debug.WriteLine("Draw second blob");
                        Point b2SmallOffset = new Point(rectOffset.X - b2.Offset.X, rectOffset.Y - b2.Offset.Y);
                        smallImg.Draw(b2.Contour, new Gray(255), new Gray(255), 0, -1, b2SmallOffset);
                    }
                }*/

                //CvInvoke.cvShowImage("smallImg", smallImg);


                bool edge = false;

                /*for (int x = 0; x < contourImg.Width; x++)
                {
                    if (contourImg.Data[contourImg.ROI.Top, contourImg.ROI.Left + x, 0] == 255)
                    {
                        //Console.WriteLine("Pixel value 255 - 1");
                        edge = true;
                    }
                }*/

                //Debug.WriteLine("edge: " + edge);

                int whitepix = 0;
                int whiteedge = 0;

                if (edge == false)
                {
                    for (int x = 0; x < contourImg.Width; x++)
                    {
                        for (int y = 0; y < contourImg.Height; y++)
                        {
                            if (contourImg.Data[contourImg.ROI.Top + y, contourImg.ROI.Left + x, 0] == 255)
                            {
                                whitepix++;
                                if (x == 0 || x == contourImg.Width - 1 || y == 0 || y == contourImg.Height - 1)
                                {
                                    whiteedge++;
                                }
                                //Console.WriteLine("Pixel value 255 - 2");
                            }
                        }
                    }
                }

                //Debug.WriteLine("edge counted");

                int boxarea = contourImg.Height * contourImg.Width;

                if (boxarea > 0)
                {
                    int whitepercent = whitepix * 100 / boxarea;

                    int boxperi = 2 * contourImg.Height + 2 * contourImg.Width;

                    int peripercent = whiteedge * 100 / boxperi;

                    //Console.WriteLine("edge: " + edge + " White percent: " + whitepercent + " Edge white percent: " + peripercent);

                    //Console.WriteLine("White percent: " + whitepercent + " Edge white percent: " + peripercent);

                    contourImg.ROI = Rectangle.Empty;

                    Random random = new Random();
                    int randomNum = random.Next(1000);
                    string name = whitepercent + "-Rect-" + randomNum.ToString() + "-" + ".png";
                    //smallImg.Save(name);
                    //Debug.WriteLine("Percent: " + whitepercent);
                    /*int keypress = CvInvoke.cvWaitKey(0);

                    if (keypress == 110)
                    {
                        falsefile.WriteLine(whitepercent);
                    }
                    else if (keypress == 121)
                    {
                        truefile.WriteLine(whitepercent);
                    }*/
                    
                    //file.WriteLine(whitepercent + (char)keypress);

                    if (edge == false && whitepercent > 15) // && peripercent < 50 && binaryCropImage.Data[bottom.Y, bottom.X, 0] == 255)
                    {
                        Blob finalBlob = new Blob();
                        finalBlob.Contour = co;
                        finalBlob.WhitePercent = whitepercent;
                        //contourImg.Draw(rect, new Gray(150), 1);
                        RectStruct contrect = new RectStruct(pixwidth, (int)pixheight, whitepercent, bottom);
                        RectList.Add(contrect);
                        finalList.Add(finalBlob);
                    }

                }
            }


            //Console.WriteLine("rectlist count: " + RectList.Count());

            int minoverlap = 45;
            List<int> DeleteList = new List<int>();

            for (int i1 = 0; i1 < RectList.Count(); i1++)
            {
                //double maxoverlap = 0;
                int indexover = 0;
                for (int i2 = 0; i2 < RectList.Count(); i2++)
                {
                    if (i1 != i2)
                    {
                        int overlap = Math.Max(0, (Math.Min(RectList[i1].pos.X + RectList[i1].width / 2, RectList[i2].pos.X + RectList[i2].width / 2) - Math.Max(RectList[i1].pos.X - RectList[i1].width / 2, RectList[i2].pos.X - RectList[i2].width / 2))) * (Math.Min(RectList[i1].pos.Y, RectList[i2].pos.Y) - Math.Max(RectList[i1].pos.Y - RectList[i1].height, RectList[i2].pos.Y - RectList[i2].height));
                        //Console.WriteLine("Height1: " + RectList[i1].height + " Height2: " + RectList[i2].height + " Width1: " + RectList[i1].width + " Width2: " + RectList[i2].width + " Pos1: " + RectList[i1].pos + " Pos2: " + RectList[i2].pos + " Overlap: " + overlap);
                        if (overlap > 0)
                        {
                            double percent1 = (double)overlap / ((double)RectList[i1].height * (double)RectList[i1].width) * 100;
                            double percent2 = (double)overlap / ((double)RectList[i2].height * (double)RectList[i2].width) * 100;
                            //Debug.WriteLine("i1 = " + i1 + " i2 = " + i2);
                            //Debug.WriteLine("Percent1: " + percent1 + " Percent2: " + percent2);

                            //if ((percent1 > minoverlap && percent1 > maxoverlap) || (percent2 > minoverlap && percent2 > maxoverlap))
                            if ((percent1 > minoverlap) || (percent2 > minoverlap))
                            {
                                //Debug.WriteLine("Overlap > min");

                                if (percent1 > percent2)
                                {
                                    //maxoverlap = percent1;
                                    contourImg.Draw(new CircleF(RectList[i1].pos, 3.0f), new Gray(150), -1);
                                }
                                else
                                {
                                    //maxoverlap = percent2;
                                    contourImg.Draw(new CircleF(RectList[i2].pos, 3.0f), new Gray(150), -1);
                                }

                                indexover = i2;
                                //Debug.WriteLine("i1 ratio: " + RectList[i1].ratio + " i2 ratio: " + RectList[i2].ratio);
                                if (RectList[i1].ratio > RectList[indexover].ratio || (RectList[i1].ratio == RectList[indexover].ratio && i1 < indexover))
                                {
                                    //Debug.WriteLine("DELETE i1");
                                    DeleteList.Add(indexover);
                                }
                                //CvInvoke.cvShowImage("Contour Image", contourImg);
                                //CvInvoke.cvWaitKey(0);

                            }
                        }
                    }
                }

                /*if(maxoverlap <= minoverlap)
		        {
                    contourImg.Draw(new Rectangle(RectList[i1].pos.X - RectList[i1].width/2,RectList[i1].pos.Y-RectList[i1].height, RectList[i1].width, RectList[i1].height),new Gray(150),2);

			        //cv::rectangle(blobframe,cv::Rect(cv::Point(rects[it].pos.x-rects[it].width/2,rects[it].pos.y),cv::Point(rects[it].pos.x+rects[it].width/2,rects[it].pos.y-rects[it].height)),150,1,8,0);
			        //counter++;
		        }
		        else if(maxoverlap > minoverlap)
		        {
			        if(RectList[i1].ratio > RectList[indexover].ratio || (RectList[i1].ratio == RectList[indexover].ratio && i1 < indexover))
			        {
				        //counter++;
                        contourImg.Draw(new Rectangle(RectList[i1].pos.X - RectList[i1].width / 2, RectList[i1].pos.Y - RectList[i1].height, RectList[i1].width, RectList[i1].height), new Gray(255), 2);
                        //cv::rectangle(blobframe,cv::Rect(cv::Point(rects[it].pos.x-rects[it].width/2,rects[it].pos.y),cv::Point(rects[it].pos.x+rects[it].width/2,rects[it].pos.y-rects[it].height)),255,1,8,0);
			        }
		        }*/
            }

            DeleteList = DeleteList.OrderByDescending(f => f).Distinct().ToList();
            

            foreach (int index in DeleteList)
            {
                finalList.RemoveAt(index);
            }

            for (int i = 0; i < finalList.Count(); i++)
            {
            
                if (finalList[i].WhitePercent <= 20 || finalList[i].WhitePercent >= 60) finalList[i].PersWeight = 0.8;
                if ((finalList[i].WhitePercent > 20 && finalList[i].WhitePercent < 30) || (finalList[i].WhitePercent > 50 && finalList[i].WhitePercent < 60)) finalList[i].PersWeight = 0.9;
                if (finalList[i].WhitePercent >= 30 && finalList[i].WhitePercent <= 50) finalList[i].PersWeight = 1.0;
                returnList.Add(finalList[i].Contour);

                contourImg.Draw(new Rectangle(finalList[i].Contour.BoundingRectangle.Left+(finalList[i].Contour.BoundingRectangle.Right - finalList[i].Contour.BoundingRectangle.Left) / 2-maxheight/6, finalList[i].Contour.BoundingRectangle.Bottom - maxheight, maxheight / 3, maxheight), new Gray(150), 1);
            }

            //for (int i = 0; i < RectList.Count(); i++)
            //{
             
            //    if (!DeleteList.Contains(i))
            //    {
            //        if(RectList[i].ratio <= 20 || RectList[i].ratio >= 60) persWeight = persWeight * 0.8;
            //        if ((RectList[i].ratio > 20 && RectList[i].ratio < 30) || (RectList[i].ratio > 50 && RectList[i].ratio < 60)) persWeight = persWeight * 0.9;
            //        if (RectList[i].ratio >= 30 && RectList[i].ratio <= 50) persWeight = persWeight * 1.0;

            //        contourImg.Draw(new Rectangle(RectList[i].pos.X - RectList[i].width / 2, RectList[i].pos.Y - RectList[i].height, RectList[i].width, RectList[i].height), new Gray(255), 1);
            //    }
            //}

                if(debugBox.Checked) CvInvoke.cvShowImage("RectContourImg", contourImg);
                //CvInvoke.cvWaitKey(0);

            contourImg.ROI = Rectangle.Empty;
            //falsefile.Close();
            //truefile.Close();
            //Debug.WriteLine("Finished");
            
            return returnList;
            //return finalList;
        }

        private void clearTrack_Click(object sender, EventArgs e)
        {
            trackworldFrame.SetZero();
        }

    }

    class GroupTuple
    {
        public double[] groupState;
        public List<PointF> personCoord;
        public int prednum;
        public double radius;

        public GroupTuple()
        {
            this.groupState = new double[5];
        }
    }

    public struct RectStruct
    {
        public int width, height, ratio;
        public Point pos;

        public RectStruct(int w, int h, int r, Point p)
        {
            width = w;
            height = h;
            ratio = r;
            pos = p;
        }
    }

    class KalmanStruct
    {
        public Kalman KF;
        public Rgb color;
        public List<double> measdist;
        public List<int> measnum;
        public Int32 label;
        public List<double> newmeasdist;
        public List<int> newmeasnum;
        public int deleted;
        public int samples;

        public KalmanStruct()
        {
            measdist = new List<double>();
            measnum = new List<int>();
            newmeasdist = new List<double>();
            newmeasnum = new List<int>();
        }
    }

    class Blob
    {
        public Contour<Point> Contour;
        public Point Offset;
        public Rectangle BoundingBox;
        public double Angle;
        public double WhitePercent;
        public double PersWeight;
    }


}
