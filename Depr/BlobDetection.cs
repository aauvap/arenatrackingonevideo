﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.Util;

namespace TrackingOneVideo
{
    partial class Form1
    {
        public void BlobDetection(Image<Gray, byte> input, out List<Point> pixcoord_T, out List<PointF> worldcoord_T, out Image<Gray, byte> blobframe_T, out bool foundblob_T)
        {
            int counter = 0;

            double area;
            Rectangle boundingbox;

            Contour<Point> contours;
            List<Contour<Point>> contList = new List<Contour<Point>>();

            pixcoord_T = new List<Point>();
            worldcoord_T = new List<PointF>();
            blobframe_T = new Image<Gray, byte>(imgWidth, imgHeight);


            contours = input.FindContours(Emgu.CV.CvEnum.CHAIN_APPROX_METHOD.CV_CHAIN_APPROX_NONE, Emgu.CV.CvEnum.RETR_TYPE.CV_RETR_EXTERNAL);

            //contours
            if (contours != null)
            {
                for (; contours != null; contours = contours.HNext) 
                {
                    boundingbox = contours.BoundingRectangle;

                    area = contours.Area;

                    int boundingY = boundingbox.Y + boundingbox.Height;

                    if (area > minArea)
                    {
                        
//#if DEBUG
//                        Console.WriteLine("Area: " + area);
//                        blobframe_T.Draw(contours, new Gray(255), 1);
//                        blobframe_T.Draw(boundingbox, new Gray(100), 1);
//#endif
                        counter++;
                        contList.Add(contours);

                        
                    }
                }
            }

            if (counter == 0) foundblob_T = false;
            else
            {
                foundblob_T = true;
                
                List<Contour<Point>> splitContours = SplitAllBlobs(contList);

                List<Contour<Point>> finalList = ContourRect(splitContours, input);
                
                foreach (Contour<Point> co in finalList)
                {
                    // CALL HOMOGRAPHY HERE TO RETURN IN BANEKOOR
                    PointF banekoor = Homography(new Point(co.BoundingRectangle.X + (co.BoundingRectangle.Width / 2), co.BoundingRectangle.Y + co.BoundingRectangle.Height));
                    

                    pixcoord_T.Add(new Point(co.BoundingRectangle.X + (co.BoundingRectangle.Width / 2), co.BoundingRectangle.Y + co.BoundingRectangle.Height));
                    worldcoord_T.Add(banekoor);
                }
            }
        }

        PointF Homography(Point inputcoord)
        {
            PointF worldcoord = new PointF(); ;
            
            Matrix<float> H = new Matrix<float>(temp_H);

            float[] temp_imagecoord = { (float)inputcoord.X, (float)inputcoord.Y, 1.0f };
            Matrix<float> imagecoord = new Matrix<float>(temp_imagecoord);

            Matrix<float> result = H.Mul(imagecoord);

            float[,] resultArray = result.Data;

            worldcoord.X = resultArray[0, 0] / resultArray[2, 0];
            worldcoord.Y = resultArray[1, 0] / resultArray[2, 0];

            return worldcoord;
        }
    }
}
