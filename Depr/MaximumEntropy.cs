﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenCvSharp;
//using OpenCvSharp.Extensions;
using OpenCvSharp.Utilities;
using OpenCvSharp.CPlusPlus;

namespace tracking
{
    public static class MaximumEntropy
    {
        public static double FindThreshold(Mat inputImg0)
        {
            // initialization:
            double threshold = -1;
            int ih, it;
            int first_bin;
            int last_bin;
            double tot_ent;			                // total entropy
            double max_ent;			                // max entropy
            double ent_back;		                // entropy of the background pixels at a given threshold
            double ent_obj;			                // entropy of the object pixels at a given threshold
            double[] norm_histo = new double[256];  // normalized histogram
            double[] P1 = new double[256];			// cumulative normalized histogram
            double[] P2 = new double[256];

            double total = 0;

            //Mat[] image = new Mat[] { inputImg0 };
            Size s = inputImg0.Size();
            
            int i,j,pixel;
            double[] histArray = new double[256];
            
            for (i = 0; i < s.Height; i++)
            {
                for (j = 0; j < s.Width; j++)
                {
                    // Generate Histogram
                    pixel = inputImg0.At<byte>(i, j);
                    histArray[pixel] = histArray[pixel]+1;

                    
                }

            }
            for (i=0;i<256;i++)
            {
                // Calculate total entropy in the image
                total += histArray[i];
            }



            // Normalize histogram by dividing with the total entropy
            for (i=0;i<256;i++)
            {
                norm_histo[i] = histArray[i] / total;
            }

            P1[0] = norm_histo[0];
            P2[0] = 1.0 - P1[0];
            for (ih = 1; ih < 256; ih++)
            {
                P1[ih] = P1[ih - 1] + norm_histo[ih]; //P1 stiger gradvist til en
                P2[ih] = 1.0 - P1[ih]; // P2 falder gradvist fra 1 til 0
            }

            /* Determine the first non-zero bin */
            first_bin = 0;
            for (ih = 0; ih < 256; ih++)
            {
                if (!(Math.Abs(P1[ih]) < 2.220446049250313E-16))
                {
                    first_bin = ih;
                    break;
                }
            }


            /* Determine the last non-zero bin */
            last_bin = 255;
            for (ih = 255; ih >= first_bin; ih--)
            {
                if (!(Math.Abs(P2[ih]) < 2.220446049250313E-16))
                {
                    last_bin = ih;
                    break;
                }
            }

            // Calculate the total entropy each gray-level
            // and find the threshold that maximizes it 
            max_ent = double.MinValue;

            for (it = first_bin; it <= last_bin; it++)
            {
                /* Entropy of the background pixels */
                ent_back = 0.0;
                for (ih = 0; ih <= it; ih++)
                {
                    if (histArray[ih] != 0)
                    {
                        ent_back -= (norm_histo[ih] / P1[it]) * Math.Log(norm_histo[ih] / P1[it]); // log = log2
                    }
                }

                /* Entropy of the object pixels */
                ent_obj = 0.0;
                for (ih = it + 1; ih < 256; ih++)
                {
                    if (histArray[ih] != 0)
                    {
                        ent_obj -= (norm_histo[ih] / P2[it]) * Math.Log(norm_histo[ih] / P2[it]);
                    }
                }

                /* Total entropy */
                tot_ent = ent_back + ent_obj;

                if (max_ent < tot_ent)
                {
                    max_ent = tot_ent;
                    threshold = it;
                }
            }


            if (max_ent < 4.1) threshold = -1;
            else if (threshold > 20) threshold = 20;
            //else threshold = threshold + 5;
            return threshold;


            ////#region Calculate Histogram
            //// Calculate histogram
            //Mat histo = new Mat();
            ////double[] hist = new double[256];
            //int[] hdims = { 256 }; // Histogram size for each dimension
            //Rangef[] ranges = { new Rangef(0, 256), }; // min/max 

            //Cv2.CalcHist(
            //    new Mat[] { inputImg0 },
            //    new int[] { 0 },
            //    null,
            //    histo,
            //    1,
            //    hdims,
            //    ranges);
            //CvHistogram test = new CvHistogram();

            ////#endregion
            ////CvHistogram test = new CvHistogram(inputImg0);
            ////int length = hist.Length;
            ////for (int i = 0; i < length; i++)
            ////{
            ////    hist[i] = histo[i];
            ////}


            //return threshold;

        }
    }
}