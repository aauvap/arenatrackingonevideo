﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;
using System.Xml.Serialization;
using System.Collections;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.Util;

namespace TrackingOneVideo
{
    public partial class Form1 : Form
    {
        class Blob
        {
            public Contour<Point> Contour;
            public Point Offset;
            public Rectangle BoundingBox;
            public double Angle;
            public double WhitePercent;
            public double PersWeight;
        }

        struct Defect
        {
            public double Depth;
            public Point DepthPoint;
            public double DepthAngle;
            public double SplitAngle;
            public Point StartPoint;
            public Point EndPoint;
        }

        private List<Contour<Point>> SplitAllBlobs(List<Contour<Point>> origContours)
        {
            List<Blob> blobList = new List<Blob>();

            foreach (var contour in origContours)
            {
                Rectangle BB = contour.BoundingRectangle;
                Point offset = BB.Location;
                int ContourYMax = contour.Max(ff => ff.Y);
                List<Point> bottomPoints = contour.Where(f => f.Y == ContourYMax).ToList();
                Point position = new Point((int)bottomPoints.Average(f => f.X), bottomPoints[0].Y);
                int maxHeight = maxheight; //cal.HeightImage.Data[position.Y, position.X, 0];
                int angle = 90; //cal.AngleImage.Data[position.Y, position.X, 0];

                Blob blob = new Blob
                {
                    Contour = contour,
                    Offset = offset,
                    Angle = angle
                };

                blobList.Add(blob);
            }

            List<Contour<Point>> finalContours = new List<Contour<Point>>();
            Image<Gray, byte> tempImg = new Image<Gray, byte>(imgWidth, imgHeight);

            foreach (Blob blob in blobList)
            {
                Image<Gray, byte> blobImage = new Image<Gray, byte>(blob.Contour.BoundingRectangle.Width + 2, blob.Contour.BoundingRectangle.Height + 2);
                blobImage.Draw(blob.Contour, new Gray(255), new Gray(255), 0, -1, new Point(-blob.Offset.X + 1, -blob.Offset.Y + 1));

                bool didSplitV = true;
                while (didSplitV)
                {
//#if DEBUG
//                    CvInvoke.cvDestroyWindow("blob 1");
//                    CvInvoke.cvShowImage("blob 1", blobImage.Ptr);
//                    CvInvoke.cvWaitKey(0);
//#endif
                    didSplitV = SplitBlobVertical(blobImage, blob);
                }

                bool didSplitH = true;
                while (didSplitH)
                {
//#if DEBUG
//                    CvInvoke.cvDestroyWindow("blob 1");
//                    CvInvoke.cvShowImage("blob 1", blobImage.Ptr);
//                    CvInvoke.cvWaitKey(0);
//#endif
                    didSplitH = SplitBlobHorizontal(blobImage, blob);
                }

                //Check all blob are good and fill final blobs
                Contour<Point> contour = blobImage.FindContours(Emgu.CV.CvEnum.CHAIN_APPROX_METHOD.CV_CHAIN_APPROX_NONE, Emgu.CV.CvEnum.RETR_TYPE.CV_RETR_EXTERNAL);
                blobImage.Dispose();
                blobImage = null;
                

                for (; contour != null; contour = contour.HNext)
                {
                    if (contour.Area > 5)
                    {
                        // have to draw and find contours again to find proper offset and angle
                        Rectangle bb = contour.BoundingRectangle;
                        Contour<Point> c = new Contour<Point>(new MemStorage());
                        int max = contour.Total;
                        for (int i = 0; i < max; i++)
                        {
                            //Debug.WriteLine("old {0}", contour[i]);
                            c.Push(new Point(contour[i].X - bb.Left, contour[i].Y - bb.Top));
                            //Debug.WriteLine("new {0}", c[i]);
                        }

                        Point newOffset = new Point(blob.Offset.X + bb.Location.X - 1, blob.Offset.Y + bb.Location.Y - 1);

                        tempImg.Draw(c, new Gray(255), new Gray(255), 0, -1,newOffset);

                        //Blob tempBlob = new Blob
                        //{
                        //    Contour = c,
                        //    Offset = newOffset,
                        //    BoundingBox = contour.BoundingRectangle,
                        //    Angle = 90 //cal.AngleImage.Data[newOffset.Y + bb.Height, newOffset.X + bb.Width / 2, 0]
                        //};
                        //finalBlobs.Add(tempBlob);
                    }
                }
            }

            Contour<Point> contours = tempImg.FindContours(Emgu.CV.CvEnum.CHAIN_APPROX_METHOD.CV_CHAIN_APPROX_NONE, Emgu.CV.CvEnum.RETR_TYPE.CV_RETR_EXTERNAL);

            if (contours != null)
            {
                for (; contours != null; contours = contours.HNext)
                {
                    finalContours.Add(contours);
                }
            }
            return finalContours;
        }

        private bool SplitBlobVertical(Image<Gray, byte> blobImage, Blob blob)
        {
            bool didASplit = false;
            Contour<Point> contour = blobImage.FindContours(Emgu.CV.CvEnum.CHAIN_APPROX_METHOD.CV_CHAIN_APPROX_NONE, Emgu.CV.CvEnum.RETR_TYPE.CV_RETR_EXTERNAL);

            //perform checks whether to split or not
            double deviation = 45;

            double radianAngle = (-blob.Angle + 180) * (Math.PI / 180);
            double radianDeviation = deviation * (Math.PI / 180);

            for (; contour != null; contour = contour.HNext)
            {
                Rectangle BB = contour.BoundingRectangle;
                Point position = new Point(blob.Offset.X + BB.Width / 2, blob.Offset.Y + BB.Height);

                //Debug.WriteLine("");
                //Debug.WriteLine("{0} < {1}", BB.Height, maxheight / 4);
                //Debug.WriteLine("{0} < {1}", contour.Perimeter / (double)(BB.Height * 2 + BB.Width * 2), 1.2);
                //Debug.WriteLine("{0} >= {1}", (double)BB.Height / (double)BB.Width, 5);

                // Check split conditions
                if (BB.Height < maxheight / 4) continue;
                if (contour.Perimeter / (double)(BB.Height * 2 + BB.Width * 2) < 1.2) continue;
                if ((double)BB.Height / (double)BB.Width >= 5) continue;

                List<Defect> defects = FindGoodDefects(contour, radianAngle, radianDeviation, false);

                //Debug.WriteLine("Found {0} contours in vertical", defects.Count);

                // go through the list and draw ONE line in the image
                if (defects.Count > 0)
                {
                    foreach (Defect defect in defects)
                    {
                        if (defect.Depth > 1)
                        {
                            // make sure both start and end point are above the depth point
                            if (defect.StartPoint.Y < defect.DepthPoint.Y && defect.EndPoint.Y < defect.DepthPoint.Y)
                            {
                                //Debug.WriteLine("I split this blob");
                                didASplit = Split(blobImage, defect);
                                //Debug.WriteLineIf(!didASplit, "SplitBlobVertical should probably have drawn something here");
                                break;
                            }
                        }
                    }
                }
            }

            return didASplit;
        }

        private bool SplitBlobHorizontal(Image<Gray, byte> blobImage, Blob blob)
        {
            bool didASplit = false;

            Contour<Point> contour = blobImage.FindContours(Emgu.CV.CvEnum.CHAIN_APPROX_METHOD.CV_CHAIN_APPROX_NONE, Emgu.CV.CvEnum.RETR_TYPE.CV_RETR_EXTERNAL);

            //perform checks whether to split or not
            double deviation = 20;

            double radianAngle = blob.Angle * (Math.PI / 180) - Math.PI / 2;
            double radianDeviation = deviation * (Math.PI / 180);

            //Debug.WriteLine("Person Angle = " + blob.Angle);
            //Debug.WriteLine("radianAngle = " + radianAngle);
            //Debug.WriteLine("radianDeviation = " + radianDeviation);

            for (; contour != null; contour = contour.HNext)
            {
                // If blob height is smaller than maxheight, continue
                int posX = blob.Offset.X + contour.BoundingRectangle.Width / 2;
                int posY = blob.Offset.Y + contour.BoundingRectangle.Height;
                int maxHeight = maxheight;
                if (!(contour.BoundingRectangle.Height > maxHeight * 1.1))
                //if (contour.BoundingRectangle.Height * 1.1 < maxHeight)
                {
                    continue;
                }

                List<Defect> defects = FindGoodDefects(contour, -radianAngle, radianDeviation, true);

                //Debug.WriteLine("Found {0} contours in horizontal", defects.Count);

                // go through the list and draw ONE line in the image
                if (defects.Count > 0)
                {
                    if (defects[0].Depth > 1)
                    {
                        foreach (Defect defect in defects)
                        {
                            //If split here, will the two new blobs be below maxHeight?
                            Rectangle bb = contour.BoundingRectangle;
                            Rectangle newUpperBlob = new Rectangle(bb.Left, bb.Top, bb.Width, defect.DepthPoint.Y - bb.Top);
                            Rectangle newLowerBlob = new Rectangle(bb.Left, defect.DepthPoint.Y, bb.Width, bb.Bottom - defect.DepthPoint.Y);

                            Point newUpperPosition = new Point(blob.Offset.X + newUpperBlob.Width / 2, blob.Offset.Y + newUpperBlob.Height);
                            Point newLowerPosition = new Point(posX, posY);

                            int upperMaxHeight = maxheight;
                            int lowerMaxHeight = maxheight;

                            //Console.WriteLine("{0} < {1} && {2} < {3}", newUpperBlob.Height, upperMaxHeight, newLowerBlob.Height, lowerMaxHeight);

                            if (bb.Height > lowerMaxHeight * 1.1) // TEMP-tal til cal er lavet rigtigt
                            {
                                if (newUpperBlob.Height < upperMaxHeight && newLowerBlob.Height < lowerMaxHeight)
                                //if (bb.Height > 40)
                                {
                                    if (Math.Abs(defect.DepthPoint.Y - defect.StartPoint.Y) + Math.Abs(defect.DepthPoint.Y - defect.EndPoint.Y) <= Math.Abs(defect.StartPoint.Y - defect.EndPoint.Y))
                                    {
                                        //Debug.WriteLine("newUpperPosition: {0}", newUpperPosition);
                                        //Debug.WriteLine("newLowerPosition: {0}", newLowerPosition);
                                        //Blobs okay, lets split
                                        didASplit = Split(blobImage, defect);
                                        //Debug.WriteLineIf(!didASplit, "SplitBlobHorizontal should probably have drawn something here");
                                        return didASplit;
                                    }
                                }
                            }
                        }

                        // If we get this far the blob is too high for being 2 persons so we just split
                        foreach (Defect defect in defects)
                        {
                            if (Math.Abs(defect.DepthPoint.Y - defect.StartPoint.Y) + Math.Abs(defect.DepthPoint.Y - defect.EndPoint.Y) <= Math.Abs(defect.StartPoint.Y - defect.EndPoint.Y))
                            {
                                didASplit = Split(blobImage, defects[0]);
                                //Debug.WriteLineIf(!didASplit, "SplitBlobHorizontal should probably have drawn something here");
                                return didASplit;
                            }
                        }
                    }
                }
            }

            return didASplit;
        }

        /// <summary>
        /// Find convexity defects in the image
        /// </summary>
        /// <param name="contour">The contour in which to find defects</param>
        /// <param name="angle">The persons angle in radians</param>
        /// <param name="deviation">The maximum deviation from the split anle in radians</param>
        /// <param name="doubleSided">Indicates whether the function can split from both sides</param>
        /// <returns>A sorted list of defects that obey the rules. Biggest depth first.</returns>
        /// 
        private List<Defect> FindGoodDefects(Contour<Point> contour, double angle, double deviation, bool doubleSided)
        {
            List<Defect> approvedDefects = new List<Defect>();

            // Get defects and order the after depth, biggest first
            List<MCvConvexityDefect> defects = contour.GetConvexityDefacts(new MemStorage(), Emgu.CV.CvEnum.ORIENTATION.CV_CLOCKWISE).OrderByDescending(f => f.Depth).ToList();

            foreach (var defect in defects)
            {
                if (defect.Depth < 1)
                {
                    continue;
                }

                //Debug.WriteLine("");
                //Debug.WriteLine("New blob");
                //Debug.WriteLine("Called by {0}", (doubleSided) ? "horizontal" : "vertical");
                //Image<Bgr, byte> imgCopy = new Image<Bgr, byte>(200, 200);
                //imgCopy.Draw(contour, new Bgr(255, 255, 255), -1);
                //imgCopy.Draw(new LineSegment2D(defect.StartPoint, defect.DepthPoint), new Bgr(0, 0, 255), 1);
                //imgCopy.Draw(new LineSegment2D(defect.EndPoint, defect.DepthPoint), new Bgr(0, 0, 255), 1);
                //imgCopy.Draw(new LineSegment2D(defect.EndPoint, defect.StartPoint), new Bgr(0, 255, 0), 1);
                //CvInvoke.cvDestroyWindow("SplitBlobVertical");
                //CvInvoke.cvShowImage("SplitBlobVertical", imgCopy.Ptr);

                int hullLineX = defect.EndPoint.X - defect.StartPoint.X;
                int hullLineY = defect.EndPoint.Y - defect.StartPoint.Y;

                double hullAngle = Math.Atan2(hullLineY, hullLineX);

                double defectAngle = hullAngle - Math.PI / 2; // minus a whole PI because PI/2 for hull to defect and PI/2 for moving 0 to the top
                double plusLimit = angle + deviation;
                double minusLimit = angle - deviation;

                if (defectAngle < -Math.PI) defectAngle = defectAngle + Math.PI * 2;
                else if (defectAngle > Math.PI) defectAngle = defectAngle - Math.PI * 2;

                double NdefectAngle = (defectAngle > Math.PI || defectAngle < -Math.PI) ? -(defectAngle - Math.PI) : defectAngle - Math.PI;
                double NplusLimit = (plusLimit > Math.PI || plusLimit < -Math.PI) ? -(plusLimit - Math.PI) : plusLimit - Math.PI;
                double NminusLimit = (minusLimit > Math.PI || minusLimit < -Math.PI) ? -(minusLimit - Math.PI) : minusLimit - Math.PI;

                //Debug.WriteLine("hullAngle = " + (hullAngle * 180 / Math.PI));
                //Debug.WriteLine("defectAngle = " + (defectAngle * 180 / Math.PI));
                //Debug.WriteLine("ProperAngle = " + (angle * 180 / Math.PI));
                //Debug.WriteLine("plusLimit = " + (plusLimit * 180 / Math.PI));
                //Debug.WriteLine("minusLimit = " + (minusLimit * 180 / Math.PI));

                if ((defectAngle > minusLimit && defectAngle < plusLimit))
                {
                    //Debug.WriteLine("VINKEL GODKENDT");
                    approvedDefects.Add(new Defect
                    {
                        Depth = defect.Depth,
                        DepthPoint = defect.DepthPoint,
                        DepthAngle = defectAngle,
                        SplitAngle = angle,
                        StartPoint = defect.StartPoint,
                        EndPoint = defect.EndPoint
                    });
                }
                else if (doubleSided)
                {
                    double sidedDefectAngle = defectAngle;
                    double sidedAngle = angle;
                    if (defectAngle < -Math.PI / 2)
                    {
                        sidedDefectAngle = defectAngle + Math.PI;
                        sidedAngle = angle + Math.PI;
                    }
                    else if (defectAngle > Math.PI / 2)
                    {
                        sidedDefectAngle = defectAngle - Math.PI;
                        sidedAngle = angle - Math.PI;
                    }

                    //Debug.WriteLine("pluslimit {0}, minuslimit {1}, sidedDefectAngle {2}", (plusLimit) * 180 / Math.PI, (minusLimit) * 180 / Math.PI, sidedDefectAngle * 180 / Math.PI);

                    if ((sidedDefectAngle < plusLimit && sidedDefectAngle > minusLimit))
                    {

                        //Debug.WriteLine("VINKEL GODKENDT AF DOUBLESIDED");
                        approvedDefects.Add(new Defect
                        {
                            Depth = defect.Depth,
                            DepthPoint = defect.DepthPoint,
                            DepthAngle = defectAngle,
                            SplitAngle = sidedAngle,
                            StartPoint = defect.StartPoint,
                            EndPoint = defect.EndPoint
                        });
                    }
                }
                //CvInvoke.cvWaitKey(0);
            }

            return approvedDefects;
        }

        private bool Split(Image<Gray, byte> image, Defect defect)
        {
            bool drawnInImage = false;
            double angle = defect.SplitAngle;
            double climb = Math.Tan(angle);
            bool left = false;

            if (angle > Math.PI / 2 || angle < -Math.PI / 2)
            {
                left = true;
            }

            int startX = defect.DepthPoint.X;
            int startY = defect.DepthPoint.Y;
            double x = 0, y = 0, step = 0;
            double delta = (climb == 0) ? 1 : Math.Abs(1 / climb);
            delta = Math.Min(delta, 1);

            if (left)
            {
                delta = -delta;
            }
            int lastX = -1, lastY = -1;

            bool bindingDirH = (climb < 1 && climb > -1) ? true : false;

            bool pointOK = true;
            while (pointOK)
            {
                x = startX + step;
                y = startY + climb * step;
                int X, Y;
                X = (int)(x + 0.5);
                Y = (int)(y + 0.5);

                if (X < image.Width && X >= 0 && Y < image.Height && Y >= 0)
                {
                    if (lastX != X || lastY != Y)
                    {
                        if (image.Data[Y, X, 0] == 255)
                        {
                            image.Data[Y, X, 0] = 0;
                            //Debug.WriteLine("Drawed in " + X + ", " + Y);

                            if (lastX != X && lastY != Y)
                            {
                                if (bindingDirH)
                                {
                                    int tempX = X - Math.Sign(delta) * 1;
                                    image.Data[Y, tempX, 0] = 0;
                                    //Debug.WriteLine("Drawed in " + tempX + ", " + Y);
                                }
                                else
                                {
                                    int tempY = Y - Math.Sign(climb) * Math.Sign(delta) * 1;
                                    image.Data[tempY, X, 0] = 0;
                                    //Debug.WriteLine("Drawed in " + X + ", " + tempY);
                                }
                                drawnInImage = true;
                            }
                        }
                        else
                        {
                            if (lastX != X && lastY != Y)
                            {
                                if (bindingDirH)
                                {
                                    int tempX = X - Math.Sign(delta) * 1;
                                    image.Data[Y, tempX, 0] = 0;
                                    //Debug.WriteLine("Drawed in " + tempX + ", " + Y);
                                }
                                else
                                {
                                    int tempY = Y - Math.Sign(climb) * Math.Sign(delta) * 1;
                                    image.Data[tempY, X, 0] = 0;
                                    //Debug.WriteLine("Drawed in " + X + ", " + tempY);
                                }
                                drawnInImage = true;
                            }
                            //Debug.WriteLine("Reached a black pixel in " + X + ", " + Y);
                            pointOK = false;

                        }
                    }
                }
                else
                {
                    //Debug.WriteLine("Reached the edge");
                    pointOK = false;
                }

                lastX = X;
                lastY = Y;

                step = step + delta;
            }
            return drawnInImage;
        }
    }
}