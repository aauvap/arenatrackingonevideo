﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.Util;

namespace TrackingOneVideo
{
    public static class MaximumEntropy
    {
        public static double FindThreshold(Image<Gray, byte> inputImg0)
        {
            Image<Gray, Byte>[] image = new Image<Gray, byte>[] { inputImg0 };
            DenseHistogram histo = new DenseHistogram(256, new RangeF(0, 255));
            double[] hist = new double[256];

            histo.Calculate(image, false, null);

            int length = hist.Length;
            for (int i = 0; i < length; i++)
            {
                hist[i] = histo[i];
            }

            // Maximum Entropy

            double threshold = -1;
            int ih, it;
            int first_bin;
            int last_bin;
            double tot_ent;			                // total entropy
            double max_ent;			                // max entropy
            double ent_back;		                // entropy of the background pixels at a given threshold
            double ent_obj;			                // entropy of the object pixels at a given threshold
            double[] norm_histo = new double[256];  // normalized histogram
            double[] P1 = new double[256];			// cumulative normalized histogram
            double[] P2 = new double[256];

            double total = 0;
            for (ih = 0; ih < 256; ih++)
            {
                total += hist[ih];
            }

            for (ih = 0; ih < 256; ih++)
            {
                norm_histo[ih] = (hist[ih]) / total;
            }

            P1[0] = norm_histo[0];
            P2[0] = 1.0 - P1[0];
            for (ih = 1; ih < 256; ih++)
            {
                P1[ih] = P1[ih - 1] + norm_histo[ih];
                P2[ih] = 1.0 - P1[ih];
            }

            /* Determine the first non-zero bin */
            first_bin = 0;
            for (ih = 0; ih < 256; ih++)
            {
                if (!(Math.Abs(P1[ih]) < 2.220446049250313E-16))
                {
                    first_bin = ih;
                    break;
                }
            }

            /* Determine the last non-zero bin */
            last_bin = 255;
            for (ih = 255; ih >= first_bin; ih--)
            {
                if (!(Math.Abs(P2[ih]) < 2.220446049250313E-16))
                {
                    last_bin = ih;
                    break;
                }
            }

            // Calculate the total entropy each gray-level
            // and find the threshold that maximizes it 
            max_ent = double.MinValue;

            for (it = first_bin; it <= last_bin; it++)
            {
                /* Entropy of the background pixels */
                ent_back = 0.0;
                for (ih = 0; ih <= it; ih++)
                {
                    if (hist[ih] != 0)
                    {
                        ent_back -= (norm_histo[ih] / P1[it]) * Math.Log(norm_histo[ih] / P1[it]); // log = log2
                    }
                }

                /* Entropy of the object pixels */
                ent_obj = 0.0;
                for (ih = it + 1; ih < 256; ih++)
                {
                    if (hist[ih] != 0)
                    {
                        ent_obj -= (norm_histo[ih] / P2[it]) * Math.Log(norm_histo[ih] / P2[it]);
                    }
                }

                /* Total entropy */
                tot_ent = ent_back + ent_obj;

                if (max_ent < tot_ent)
                {
                    max_ent = tot_ent;
                    threshold = it;
                }
            }

            if (max_ent < 4.1) threshold = -1;
            else if (threshold > 20) threshold = 20;
            //else threshold = threshold + 5;
            return threshold;
        }
    }
}
